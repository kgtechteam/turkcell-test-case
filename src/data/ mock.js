/* eslint-disable import/prefer-default-export */
export const packages = [
  {
    name: "Platinum 8",
    color:
      "#009ed8 radial-gradient(circle at 40px 0, rgba(0,179,227,1) 40%, rgba(0,179,227,0.75) 40%, rgba(0,179,227,0.75) 60%, rgba(0,179,227,0.5) 60%, rgba(0,179,227,0.5) 85%, rgba(0,0,0,0) 85%)",
    price: "40",
    internet: {
      main: 8,
      addition: 2
    },
    call: 500,
    services: ["bip", "fizy", "abroad", "lifebox", "tv"]
  },
  {
    name: "Platinum 16",
    color:
      "rgba(246, 175, 33, 1) radial-gradient(circle at 40px 0, #f68b21 40%, rgba(246, 139, 33, 0.75) 40%, rgba(246, 139, 33, 0.75) 60%, rgba(246, 139, 33, 0.5) 60%, rgba(246, 139, 33, 0.5) 85%, rgba(0,0,0,0) 85%)",
    price: "29",
    internet: {
      main: 16,
      addition: 2
    },
    call: 500,
    services: ["bip", "fizy", "abroad", "lifebox", "tv"]
  },
  {
    name: "Platinum 32",
    color:
      "rgb(104, 113, 102) radial-gradient(circle at 40px 0, #54585a 40%, rgba(84, 88, 90, 0.75) 40%, rgba(84, 88, 90, 0.75) 60%, rgba(84, 88, 90, 0.5) 60%, rgba(84, 88, 90, 0.5) 85%, rgba(0, 0, 0, 0) 85%)",
    price: "89",
    internet: {
      main: 32,
      addition: 2
    },
    call: 500,
    services: ["bip", "fizy", "abroad", "lifebox", "tv"]
  }
]

export const aboutContent = [
  {
    subTitle: "Lorem ipsum dolor sit amet",
    summary:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa.",
    longText:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa.,Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa."
  },
  {
    subTitle: "Lorem ipsum dolor sit amet",
    summary:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa.",
    longText:
      "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa.,Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa."
  }
]

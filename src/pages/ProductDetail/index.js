/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React, { PureComponent } from "react"
import { BrowserView, MobileView } from "react-device-detect"
import Layout from "layouts/MainLayoutV2"
import "../../components/organism/HeaderV2/index.scss"
import "./index.scss"

import ProductDetailPackages from "components/molecules/ProductDetailPackages"
import ProductDetailCheckbox from "components/molecules/ProductDetailCheckbox"
import {
  ProductDetailInfo,
  ProductDetailSubRow,
  ProductDetailBenefits,
  ProductDetailBanner,
  ProductDetailVideo,
  ProductDetailMoreInfo,
  ProductDetailCarousel,
  ProductDetailAccordion,
  ProductDetailTariffs,
  ProductDetailMoreInfoMobile,
  ProductDetailVideoMobile,
  ProductDetailAccordionMobile,
  ProductDetailFloatingBar,
  ProductDetailFixedBottomBar
} from "../../components/molecules"

export default class ProductDetail extends PureComponent {
  render() {
    return (
      <Layout>
        <BrowserView>
          <main className="product-detail-area">
            <ProductDetailInfo />
            <ProductDetailSubRow />
            <section id="product-detail-benefits" className="p-t-10 p-b-20">
              <div className="container">
                <div className="clearfix m-flex justify-content-center align-items-center">
                  <div className="visual text-center">
                    <img src="/static/media/new-phone.182a72db.png" alt="" />
                  </div>
                  <div className="text-left">
                    <h2>Paketteki Faydalar</h2>
                    <ProductDetailBenefits />
                  </div>
                </div>
              </div>
            </section>
            <ProductDetailBanner />
            <ProductDetailVideo />
            <ProductDetailMoreInfo />
            <ProductDetailCarousel />
            <ProductDetailAccordion />
          </main>
        </BrowserView>

        <MobileView>
          <main className="product-detail-mobile-area">
            <div id="product-detail" className="product-detail">
              <div className="product-detail-top">
                <h1>
                  Platinium Maksi 16 GB
                  <i className="a-icon icon-arrow-right" />
                </h1>
                <div className="a-ribbon a-ribbon--a">12 Ayda Öde</div>

                <div className="container">
                  <ProductDetailTariffs className="m-t-50" />

                  <span className="m-t-50 block">
                    <strong>Paket Kapsamı</strong>
                  </span>

                  <ProductDetailPackages />
                </div>

                <div
                  id="product-detail-subrow"
                  className="bg_color_haze p-t-30 p-b-30 m-t-20 text-center"
                >
                  <small className="font-400 block m-b-0 p-l-20 p-r-20">
                    Lorem ipsum dolor sit amet consectetuer. Lorem ipsum dolor
                    sit amet consectetuer. Lorem ipsum
                  </small>
                </div>

                <div className="container p-b-30">
                  <ProductDetailCheckbox />
                </div>

                <section id="product-detail-benefits" className="p-b-20">
                  <div className="container">
                    <div className="clearfix m-purchase-features p-t-25 m-flex justify-content-center align-items-center">
                      <div className="text-left">
                        <h2 className="text-center block">
                          Paketteki Faydalar
                        </h2>
                        <ProductDetailBenefits />
                      </div>
                      <div className="visual text-center">
                        <img
                          src="/static/media/new-phone.182a72db.png"
                          alt=""
                        />
                      </div>
                    </div>
                  </div>
                </section>

                <ProductDetailVideoMobile />
                <ProductDetailMoreInfoMobile />
                <ProductDetailFloatingBar />
                <ProductDetailAccordionMobile />
                <ProductDetailFixedBottomBar />
              </div>
            </div>
          </main>
        </MobileView>
      </Layout>
    )
  }
}

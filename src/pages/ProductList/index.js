/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React, { PureComponent } from "react"
import { BrowserView, MobileView } from "react-device-detect"
import Layout from "layouts/MainLayout"
import {
  ProductListTabs,
  Carousel,
  Cards,
  AboutSection,
  WhatWeHave,
  TabSliderMobile,
  CarouselMobile,
  CardsMobile
} from "components/molecules"
import { Title, Button, Seperator } from "components/atoms"
import { packages, aboutContent } from "data/ mock"

export default class ProductList extends PureComponent {
  handleChange = e => {
    console.log(e.currentTarget.name)
  }

  render() {
    return (
      <Layout>
        <main className="product-list-area">
          <BrowserView>
            <ProductListTabs />
            <section className="white m-b-0" id="container">
              <div className="container">
                <Title as="h2" className="text-center">
                  Faturalı Paketler
                </Title>
                <div className="product-list__bar">
                  <div className="m-btn-group justify-content-start product-list__filter">
                    <a
                      className="a-btn a-btn--with-icon icon-close a-btn--tag a-btn--tag--transparent js-filter-clear"
                      href="javascript:;"
                      style={{ display: "inline" }}
                    >
                      Tüm Filtreleri Temizle
                    </a>
                    <a
                      className="a-btn a-btn--with-icon icon-close a-btn--tag a-btn--tag--white js-filter-button"
                      href="javascript:;"
                      data-value="Apple"
                    >
                      Apple
                    </a>
                  </div>
                  <div
                    className="a-checkbox-switch"
                    data-component="CheckboxSwitch"
                  >
                    <span className="a-checkbox-switch__text">
                      Karşılaştırma Modu
                    </span>
                    <label
                      className="a-checkbox-switch-wrap"
                      htmlFor="s0-0-0-7-13-name"
                    >
                      <input type="checkbox" id="s0-0-0-7-13-name" />
                      <span className="a-checkbox-switch-wrap__slider a-checkbox-switch-wrap__round" />
                    </label>
                    <div className="a-checkbox-switch__notification">
                      Listeden karşılaştırmak istediğiniz ürünleri seçin.
                    </div>
                  </div>
                </div>
                <div className="m-grid">
                  <div className="m-grid-col-3">
                    <div className="o-filter-result-count m-b-20">
                      106 Sonuç
                    </div>
                    <div className="o-filter-side-bar">
                      <div className="o-filter-side-bar__box">
                        <div className="m-product-sort">
                          <div
                            className="m-accordion m-accordion--bordered"
                            data-component='{"showOnlyOne":true,"type":"Accordion"}'
                          >
                            <div className="m-accordion__container">
                              <div className="m-accordion__head m-accordion__head--active">
                                <i />
                                <span>Paketler (106 Sonuç)</span>
                              </div>
                              <div className="m-accordion__body">
                                <label
                                  className="a-radio a-radio--checked"
                                  htmlFor="s0-0-0-7-13-15-4-1-5-radio"
                                >
                                  <input
                                    type="radio"
                                    id="s0-0-0-7-13-15-4-1-5-radio"
                                    name="sort"
                                    data-parsley-error-message="Lütfen en az birini seçiniz."
                                    onChange={this.handleChange}
                                    value="main"
                                  />
                                  <span>Ana Paketler</span>
                                </label>
                                <label
                                  className="a-radio"
                                  htmlFor="s0-0-0-7-13-15-4-1-12-radio"
                                >
                                  <input
                                    type="radio"
                                    id="s0-0-0-7-13-15-4-1-12-radio"
                                    name="sort"
                                    data-parsley-error-message="Lütfen en az birini seçiniz."
                                    onChange={this.handleChange}
                                    value="addition"
                                  />
                                  <span>Ek Paketler</span>
                                </label>
                                <label
                                  className="a-radio"
                                  htmlFor="s0-0-0-7-13-15-4-1-10-radio"
                                >
                                  <input
                                    type="radio"
                                    id="s0-0-0-7-13-15-4-1-10-radio"
                                    name="sort"
                                    data-parsley-error-message="Lütfen en az birini seçiniz."
                                    onChange={this.handleChange}
                                    value="abroad"
                                  />
                                  <span>Yurtdışı Paketleri</span>
                                </label>
                                <label
                                  className="a-radio"
                                  htmlFor="s0-0-0-7-13-15-4-1-11-radio"
                                >
                                  <input
                                    type="radio"
                                    id="s0-0-0-7-13-15-4-1-11-radio"
                                    name="sort"
                                    data-parsley-error-message="Lütfen en az birini seçiniz."
                                    onChange={this.handleChange}
                                    value="tablet"
                                  />
                                  <span>Tablet / VINN WIFI</span>
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="o-filter-side-bar__box">
                        <div className="m-product-sort">
                          <div
                            className="m-accordion m-accordion--bordered"
                            data-component='{"showOnlyOne":true,"type":"Accordion"}'
                          >
                            <div className="m-accordion__container">
                              <div className="m-accordion__head">
                                <i />
                                <span>Ek Paketler (106 Sonuç)</span>
                              </div>
                              <div className="m-accordion__body">
                                <label
                                  className="a-checkbox"
                                  htmlFor="apple"
                                  data-component="FormCheckbox"
                                >
                                  <input
                                    type="checkbox"
                                    id="apple"
                                    name="sort"
                                    value="Hepsi"
                                  />
                                  <span>Ek İnternet Paketi</span>
                                </label>
                                <label
                                  className="a-checkbox"
                                  htmlFor="asus"
                                  data-component="FormCheckbox"
                                >
                                  <input
                                    type="checkbox"
                                    id="asus"
                                    name="sort"
                                    value="0-500 TL"
                                  />
                                  <span>Gaming Paketi</span>
                                </label>
                                <label
                                  className="a-checkbox"
                                  htmlFor="gm"
                                  data-component="FormCheckbox"
                                >
                                  <input
                                    type="checkbox"
                                    id="gm"
                                    name="sort"
                                    value="500-750 TL"
                                  />
                                  <span>Taraftar Paketi</span>
                                </label>
                                <label
                                  className="a-checkbox"
                                  htmlFor="apple"
                                  data-component="FormCheckbox"
                                >
                                  <input
                                    type="checkbox"
                                    id="apple"
                                    name="sort"
                                    value="750-1.000 TL"
                                  />
                                  <span>Hotspot İnternet Paketi</span>
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div className="o-filter-side-bar__box">
                        <div className="m-product-sort">
                          <div
                            className="m-accordion m-accordion--bordered"
                            data-component='{"showOnlyOne":true,"type":"Accordion"}'
                          >
                            <div className="m-accordion__container">
                              <div className="m-accordion__head m-accordion__head--active">
                                <i />
                                <span>Sırala</span>
                              </div>
                              <div className="m-accordion__body">
                                <label
                                  className="a-radio a-radio--checked"
                                  htmlFor="s0-0-0-7-13-15-4-1-5-radio"
                                >
                                  <input
                                    type="radio"
                                    id="s0-0-0-7-13-15-4-1-5-radio"
                                    name="sort"
                                    data-parsley-error-message="Lütfen en az birini seçiniz."
                                    onChange={this.handleChange}
                                    value="main"
                                  />
                                  <span>Öne Çıkanlar</span>
                                </label>
                                <label
                                  className="a-radio"
                                  htmlFor="s0-0-0-7-13-15-4-1-12-radio"
                                >
                                  <input
                                    type="radio"
                                    id="s0-0-0-7-13-15-4-1-12-radio"
                                    name="sort"
                                    data-parsley-error-message="Lütfen en az birini seçiniz."
                                    onChange={this.handleChange}
                                    value="addition"
                                  />
                                  <span>En Yeniler</span>
                                </label>
                                <label
                                  className="a-radio"
                                  htmlFor="s0-0-0-7-13-15-4-1-10-radio"
                                >
                                  <input
                                    type="radio"
                                    id="s0-0-0-7-13-15-4-1-10-radio"
                                    name="sort"
                                    data-parsley-error-message="Lütfen en az birini seçiniz."
                                    onChange={this.handleChange}
                                    value="abroad"
                                  />
                                  <span>Fiyata Göre Artan</span>
                                </label>
                                <label
                                  className="a-radio"
                                  htmlFor="s0-0-0-7-13-15-4-1-11-radio"
                                >
                                  <input
                                    type="radio"
                                    id="s0-0-0-7-13-15-4-1-11-radio"
                                    name="sort"
                                    data-parsley-error-message="Lütfen en az birini seçiniz."
                                    onChange={this.handleChange}
                                    value="tablet"
                                  />
                                  <span>Fiyata Göre Azalan</span>
                                </label>
                                <label
                                  className="a-radio"
                                  htmlFor="s0-0-0-7-13-15-4-1-11-radio"
                                >
                                  <input
                                    type="radio"
                                    id="s0-0-0-7-13-15-4-1-11-radio"
                                    name="sort"
                                    data-parsley-error-message="Lütfen en az birini seçiniz."
                                    onChange={this.handleChange}
                                    value="tablet"
                                  />
                                  <span>İndirimler</span>
                                </label>
                                <label
                                  className="a-radio"
                                  htmlFor="s0-0-0-7-13-15-4-1-11-radio"
                                >
                                  <input
                                    type="radio"
                                    id="s0-0-0-7-13-15-4-1-11-radio"
                                    name="sort"
                                    data-parsley-error-message="Lütfen en az birini seçiniz."
                                    onChange={this.handleChange}
                                    value="tablet"
                                  />
                                  <span>En Çok Satanlar</span>
                                </label>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="m-grid-col-9">
                    <Carousel />
                    <Title as="h5" className="text-center">
                      Ek Paketler
                    </Title>
                    <div className="m-flex">
                      {packages.map(packageItem => (
                        <Cards packageItem={packageItem} />
                      ))}
                    </div>
                    <div className="text-center m-50">
                      <Button secondary>Tüm Faturalı Paketler</Button>
                    </div>
                  </div>
                </div>
                <Seperator />
                <AboutSection
                  title="Hediye Kutum Hakkında"
                  content={aboutContent}
                />
              </div>
              <WhatWeHave />
            </section>
          </BrowserView>
          <MobileView>
            <Title as="h4">Cihazlar</Title>
            <TabSliderMobile />
            <CarouselMobile />
            <section id="product-list-action" className="product-list-action">
              <div className="o-list-action-bar" data-component="ListActionBar">
                <div
                  className="o-list-action-bar__group"
                  style={{ justifyContent: "flex-end" }}
                >
                  <div className="o-list-action-bar__buttons">
                    <a
                      href="javascript:;"
                      className="icon-filter js-bar-filter"
                    >
                      Filtrele <span>0</span>
                    </a>
                    <a href="javascript:;" className="icon-swap js-bar-sort">
                      Sırala{" "}
                      <select>
                        <option>deneme</option>
                        <option>deneme</option>
                        <option>deneme</option>
                        <option>deneme</option>
                        <option>deneme</option>
                      </select>
                    </a>
                  </div>
                </div>
                <div
                  className="m-product-filter-mobile"
                  data-component="ProductFilterMobile"
                >
                  <div className="m-product-filter-mobile__close">
                    <i className="a-icon icon-close" />
                  </div>
                  <div className="m-product-filter-mobile__title">Filtrele</div>
                  <div className="m-product-filter-mobile__accordion">
                    <div
                      className="m-accordion"
                      data-component='{"showOnlyOne":false,"type":"Accordion"}'
                    >
                      <div className="m-accordion__container m-accordion--scrolled">
                        <div className="m-accordion__head">
                          <i />
                          <span>Markalar</span>
                        </div>
                        <div className="m-accordion__body">
                          <div className="m-product-filter__input">
                            <a
                              className="a-btn-icon m-product-filter-input__icon"
                              href="javascript:;"
                              title="Ara"
                            >
                              <i className="icon-search" />
                            </a>
                            <input
                              type="text"
                              placeholder="Marka Ara"
                              name="m-product-filter-input"
                              autoComplete="off"
                            />
                          </div>
                          <div className="m-accordion__scroll-container">
                            <label
                              className="a-checkbox"
                              htmlFor="apple"
                              data-component="FormCheckbox"
                            >
                              <input
                                type="checkbox"
                                id="apple"
                                name="sort"
                                value="Apple"
                              />
                              <span>Apple (40)</span>
                            </label>
                            <label
                              className="a-checkbox"
                              htmlFor="asus"
                              data-component="FormCheckbox"
                            >
                              <input
                                type="checkbox"
                                id="asus"
                                name="sort"
                                value="Asus"
                              />
                              <span>Asus (15)</span>
                            </label>
                            <label
                              className="a-checkbox"
                              htmlFor="gm"
                              data-component="FormCheckbox"
                            >
                              <input
                                type="checkbox"
                                id="gm"
                                name="sort"
                                value="General Mobile"
                              />
                              <span>General Mobile (11)</span>
                            </label>
                            <label
                              className="a-checkbox"
                              htmlFor="apple11"
                              data-component="FormCheckbox"
                            >
                              <input
                                type="checkbox"
                                id="apple11"
                                name="sort"
                                value="Apple"
                              />
                              <span>Apple (40)</span>
                            </label>
                            <label
                              className="a-checkbox"
                              htmlFor="asus12"
                              data-component="FormCheckbox"
                            >
                              <input
                                type="checkbox"
                                id="asus12"
                                name="sort"
                                value="Asus"
                              />
                              <span>Asus (15)</span>
                            </label>
                            <label
                              className="a-checkbox"
                              htmlFor="gm13"
                              data-component="FormCheckbox"
                            >
                              <input
                                type="checkbox"
                                id="gm13"
                                name="sort"
                                value="General Mobile"
                              />
                              <span>General Mobile (11)</span>
                            </label>
                            <label
                              className="a-checkbox"
                              htmlFor="apple14"
                              data-component="FormCheckbox"
                            >
                              <input
                                type="checkbox"
                                id="apple14"
                                name="sort"
                                value="Apple"
                              />
                              <span>Apple (40)</span>
                            </label>
                            <label
                              className="a-checkbox"
                              htmlFor="asus15"
                              data-component="FormCheckbox"
                            >
                              <input
                                type="checkbox"
                                id="asus15"
                                name="sort"
                                value="Asus"
                              />
                              <span>Asus (15)</span>
                            </label>
                            <label
                              className="a-checkbox"
                              htmlFor="gm16"
                              data-component="FormCheckbox"
                            >
                              <input
                                type="checkbox"
                                id="gm16"
                                name="sort"
                                value="General Mobile"
                              />
                              <span>General Mobile (11)</span>
                            </label>
                            <label
                              className="a-checkbox"
                              htmlFor="apple17"
                              data-component="FormCheckbox"
                            >
                              <input
                                type="checkbox"
                                id="apple17"
                                name="sort"
                                value="Apple"
                              />
                              <span>Apple (40)</span>
                            </label>
                            <label
                              className="a-checkbox"
                              htmlFor="asus18"
                              data-component="FormCheckbox"
                            >
                              <input
                                type="checkbox"
                                id="asus18"
                                name="sort"
                                value="Asus"
                              />
                              <span>Asus (15)</span>
                            </label>
                            <label
                              className="a-checkbox"
                              htmlFor="gm19"
                              data-component="FormCheckbox"
                            >
                              <input
                                type="checkbox"
                                id="gm19"
                                name="sort"
                                value="General Mobile"
                              />
                              <span>General Mobile (11)</span>
                            </label>
                          </div>
                        </div>
                      </div>
                      <div className="m-accordion__container">
                        <div className="m-accordion__head">
                          <i />
                          <span>Fiyat</span>
                        </div>
                        <div className="m-accordion__body">
                          <label
                            className="a-checkbox"
                            htmlFor="apple1"
                            data-component="FormCheckbox"
                          >
                            <input
                              type="checkbox"
                              id="apple1"
                              name="sort"
                              value="Hepsi"
                            />
                            <span>Hepsi (32)</span>
                          </label>
                          <label
                            className="a-checkbox"
                            htmlFor="asus2"
                            data-component="FormCheckbox"
                          >
                            <input
                              type="checkbox"
                              id="asus2"
                              name="sort"
                              value="0-500 TL"
                            />
                            <span>0-500 TL (5)</span>
                          </label>
                          <label
                            className="a-checkbox"
                            htmlFor="gm3"
                            data-component="FormCheckbox"
                          >
                            <input
                              type="checkbox"
                              id="gm3"
                              name="sort"
                              value="500-750 TL"
                            />
                            <span>500-750 TL (8)</span>
                          </label>
                          <label
                            className="a-checkbox"
                            htmlFor="apple4"
                            data-component="FormCheckbox"
                          >
                            <input
                              type="checkbox"
                              id="apple4"
                              name="sort"
                              value="750-1.000 TL"
                            />
                            <span>750-1.000 TL (6)</span>
                          </label>
                          <label
                            className="a-checkbox"
                            htmlFor="gm5"
                            data-component="FormCheckbox"
                          >
                            <input
                              type="checkbox"
                              id="gm5"
                              name="sort"
                              value="1.000 TL ve Üzeri"
                            />
                            <span>1.000 TL ve Üzeri (12)</span>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div className="m-product-filter-mobile__bar">
                    <a
                      className="a-btn a-btn--with-icon icon-close a-btn--tag a-btn--tag--transparent js-filter-clear"
                      href="javascript:;"
                    >
                      Filtreleri Temizle
                    </a>
                    <a className="a-btn js-filter-apply" href="javascript:;">
                      Uygula
                    </a>
                  </div>
                </div>
              </div>
            </section>
            <section id="product-list-bar" className="m-b-0">
              <div className="container">
                <div className="product-list__bar p-t-0">
                  <span>6 Ürün Listeleniyor</span>
                  <a
                    style={{ top: 0 }}
                    className="a-btn-icon"
                    data-component="ChangeGridView"
                    data-view="grid"
                    data-target="#product-list-cards"
                    title="Liste Olarak Görüntüle"
                    data-grid-title="Grid Olarak Görüntüle"
                    data-list-title="Liste Olarak Görüntüle"
                    href="javascript:;"
                  >
                    <i className="icon-as-list" />
                    grid view
                  </a>
                </div>
              </div>
            </section>
            <Title as="h5" className="text-center">
              Tüm Paketler
            </Title>
            {packages.map(packageItem => (
              <CardsMobile packageItem={packageItem} />
            ))}
          </MobileView>
        </main>
      </Layout>
    )
  }
}

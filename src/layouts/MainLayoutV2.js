/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
import React from "react"
import { BrowserView, MobileView } from "react-device-detect"
import { HeaderV2, HeaderV2Mobile, Footer } from "components/organism"

const MainLayout = ({ children }) => {
  return (
    <div>
      <BrowserView>
        <HeaderV2 />
        <main>{children}</main>
        <Footer />
      </BrowserView>

      <MobileView>
        <HeaderV2Mobile />
        <main>{children}</main>
      </MobileView>
    </div>
  )
}

export default MainLayout

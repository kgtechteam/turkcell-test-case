/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
import React from "react"
import { BrowserView, MobileView } from "react-device-detect"
import { Header, Footer, HeaderMobile, FooterMobile } from "components/organism"

const MainLayout = ({ children }) => {
  return (
    <div>
      <BrowserView>
        <Header />
        <main>{children}</main>
        <Footer />
      </BrowserView>
      <MobileView>
        <HeaderMobile />
        <main>{children}</main>
        <FooterMobile />
      </MobileView>
    </div>
  )
}

export default MainLayout

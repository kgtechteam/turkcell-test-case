/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
import React from "react"

const Title = ({ as, children, ...rest }) => {
  switch (as) {
    case "h1":
      return <h1 {...rest}>{children}</h1>
    case "h2":
      return <h2 {...rest}>{children}</h2>
    case "h3":
      return <h3 {...rest}>{children}</h3>
    case "h4":
      return <h4 {...rest}>{children}</h4>
    case "h5":
      return <h5 {...rest}>{children}</h5>
    case "h6":
      return <h6 {...rest}>{children}</h6>
    default:
      return <h3 {...rest}>{children}</h3>
  }
}

export default Title

/* eslint-disable react/jsx-props-no-spreading */
/* eslint-disable react/prop-types */
import React from "react"

const Button = ({ secondary, children, ...rest }) => {
  return (
    <button
      {...rest}
      className={`a-btn ${secondary ? "a-btn--secondary" : ""}`}
      type="button"
    >
      {children}
    </button>
  )
}
export default Button

/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */
import React from "react"

import CarouselLogo1 from "../../../assets/images/dummy/akilli-bulut.png"
import CarouselLogo2 from "../../../assets/images/dummy/gelecegi-yazanlar.png"
import CarouselLogo3 from "../../../assets/images/dummy/tedarikci.png"
import CarouselLogo4 from "../../../assets/images/dummy/medya.png"
import CarouselLogo5 from "../../../assets/images/dummy/partner-network.png"
import CarouselLogo6 from "../../../assets/images/dummy/lifecell-logo.png"
import CarouselLogo7 from "../../../assets/images/dummy/fizy-logo.png"
import CarouselLogo8 from "../../../assets/images/dummy/upcall-logo.png"
import CarouselLogo9 from "../../../assets/images/dummy/tv-plus-logo.png"
import CarouselLogo10 from "../../../assets/images/dummy/paycell-logo.png"
import CarouselLogo11 from "../../../assets/images/dummy/bip-logo.png"
import CarouselLogo12 from "../../../assets/images/dummy/gnc-logo.png"
import CarouselLogo13 from "../../../assets/images/dummy/superonline-logo.png"

const Footer = () => {
  return (
    <footer className="o-footer" data-component="Footer">
      <div className="o-footer-top">
        <div className="container">
          <div className="m-grid">
            <div className="m-grid-col">
              <ul className="a-breadcrumb" aria-label="Breadcrumb">
                <li>
                  <a href="javascript:;" title="Ana Sayfa">
                    Ana Sayfa
                  </a>
                </li>
                <li>
                  <a href="javascript:;" title="Cihazlar">
                    Cihazlar
                  </a>
                </li>
                <li>
                  <a
                    href="javascript:;"
                    aria-current="page"
                    title="Akıllı Telefonlar"
                  >
                    Akıllı Telefonlar
                  </a>
                </li>
              </ul>
            </div>
          </div>

          <nav className="o-footer-nav">
            <div className="m-grid">
              <div className="m-grid-col">
                <h3>
                  {" "}
                  <a href="javascript:;">Hakkımızda</a>
                </h3>
                <ul>
                  <li>
                    <a href="javascript:;" title="Duyurular">
                      Duyurular
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Yatırımcı İlişkileri">
                      Yatırımcı İlişkileri
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Sosyal Sorumluluk">
                      Sosyal Sorumluluk
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Sponsorluk ve STK İlişkileri">
                      Sponsorluk ve STK İlişkileri
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="İnsan Kaynakları">
                      İnsan Kaynakları
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Logolarımız">
                      Logolarımız
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Video Galeri">
                      Video Galeri
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Bilgi Toplumu Hizmetleri">
                      Bilgi Toplumu Hizmetleri
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="İletişim">
                      İletişim
                    </a>
                  </li>
                </ul>
              </div>
              <div className="m-grid-col">
                <h3>
                  {" "}
                  <a href="javascript:;">Popüler linkler</a>
                </h3>
                <ul>
                  <li>
                    <a href="javascript:;" title="Cihazlar">
                      Cihazlar
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Paketler">
                      Paketler
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Dijital Servisler">
                      Dijital Servisler
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Ev Çözümleri">
                      Ev Çözümleri
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Fırsatlar">
                      Fırsatlar
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Engel Tanımayanlar">
                      Engel Tanımayanlar
                    </a>
                  </li>
                </ul>
              </div>
              <div className="m-grid-col">
                <h3>
                  {" "}
                  <a href="javascript:;">Müşteri Hizmetleri</a>
                </h3>
                <ul>
                  <li>
                    <a href="javascript:;" title="Cihaz Ayar Desteği">
                      Cihaz Ayar Desteği
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Sipariş Takibi">
                      Sipariş Takibi
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="İade Talebi">
                      İade Talebi
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Puk Sorgulama">
                      Puk Sorgulama
                    </a>
                  </li>
                  <li>
                    <a
                      href="javascript:;"
                      title="Alacak Sorgulama / TL İade Talep"
                    >
                      Alacak Sorgulama / TL İade Talep
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Şikayet Talebi Oluşturma">
                      Şikayet Talebi Oluşturma
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Şikayet Takibi">
                      Şikayet Takibi
                    </a>
                  </li>
                </ul>
              </div>
              <div className="m-grid-col">
                <h3>
                  {" "}
                  <a href="javascript:;">Lorem Ipsum</a>
                </h3>
                <ul>
                  <li>
                    <a href="javascript:;" title="Lorem Ipsum Dolor">
                      Lorem Ipsum Dolor
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Sit Amet">
                      Sit Amet
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Consectuar">
                      Consectuar
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Lorem Ipsum Dolor">
                      Lorem Ipsum Dolor
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Sit Amet">
                      Sit Amet
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Consectuar">
                      Consectuar
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Dolor Dolor">
                      Dolor Dolor
                    </a>
                  </li>
                </ul>
              </div>
              <div className="m-grid-col">
                <h3>
                  {" "}
                  <a href="javascript:;">Size En Yakın Mağaza</a>
                </h3>
                <div className="o-footer-contact">
                  <address>
                    <div className="o-footer-contact__address">
                      <p>
                        <span>Mall Of İstanbul,</span> Süleyman Demirel Bulvarı,
                        Mahmutbey Başakşehir
                      </p>
                    </div>
                    <div className="o-footer-contact__telephone">
                      <a href="tel:02128010065" title="Telefon">
                        (0212) 801 00 65
                      </a>
                    </div>
                  </address>

                  <div className="o-footer-communication">
                    <ul>
                      <li>
                        <a href="javascript:;" title="Yol Tarifi Al">
                          Yol Tarifi Al
                        </a>
                      </li>
                      <li>
                        <a href="javascript:;" title="Randevu Al">
                          Randevu Al
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </nav>
          <hr className="o-footer__line" />
        </div>
      </div>
      <div className="o-footer-middle">
        <div className="container">
          <div className="m-grid">
            <div className="m-grid-col">
              <ul className="o-footer__language">
                <li>
                  <a href="javascript:;" title="English">
                    English
                  </a>
                </li>
                <li className="o-footer-language__active">
                  <a href="javascript:;" title="Türkçe">
                    Türkçe
                  </a>
                </li>
                <li>
                  <a href="javascript:;" title="عربى">
                    عربى
                  </a>
                </li>
                <li>
                  <a href="javascript:;" title="русский">
                    русский
                  </a>
                </li>
              </ul>
            </div>
            <div className="m-grid-col m-grid-offset-5">
              <div className="o-footer__social">
                <strong>Bizi Takip Edin</strong>
                <div className="m-btn-group m-btn-group--align-right">
                  <a className="a-btn-icon" href="javascript:;" title="Twitter">
                    <i className="icon-twitter" />
                  </a>
                  <a
                    className="a-btn-icon"
                    href="javascript:;"
                    title="Facebook"
                  >
                    <i className="icon-facebook" />
                  </a>
                  <a
                    className="a-btn-icon"
                    href="javascript:;"
                    title="Instagram"
                  >
                    <i className="icon-instagram" />
                  </a>
                  <a className="a-btn-icon" href="javascript:;" title="Youtube">
                    <i className="icon-youtube" />
                  </a>
                  <a
                    className="a-btn-icon"
                    href="javascript:;"
                    title="LinkedIn"
                  >
                    <i className="icon-linkedin" />
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="o-footer-bottom">
        <div className="container">
          <div className="o-footer__partners">
            <div
              className="m-slider"
              data-component='{"slidesPerView":"auto","spaceBetween":32,"centeredSlides":false,"navigation":true,"type":"Slider"}'
            >
              <div className="m-slider__swiper swiper-container">
                <div className="swiper-wrapper">
                  <div className="swiper-slide swiper-lazy">
                    <a href="" title="Turkcell Akıllı Bulut">
                      <figure>
                        <img
                          src={CarouselLogo1}
                          alt="Turkcell Akıllı Bulut"
                          width="171"
                        />
                      </figure>
                    </a>
                  </div>
                  <div className="swiper-slide swiper-lazy">
                    <a href="" title="Turkcell Geleceği Yazanlar">
                      <figure>
                        <img
                          src={CarouselLogo2}
                          alt="Turkcell Geleceği Yazanlar"
                          width="222"
                        />
                      </figure>
                    </a>
                  </div>
                  <div className="swiper-slide swiper-lazy">
                    <a href="" title="Turkcell Tedarikçi">
                      <figure>
                        <img
                          src={CarouselLogo3}
                          alt="Turkcell Tedarikçi"
                          width="212"
                        />
                      </figure>
                    </a>
                  </div>
                  <div className="swiper-slide swiper-lazy">
                    <a href="" title="Turkcell Medya">
                      <figure>
                        <img
                          src={CarouselLogo4}
                          alt="Turkcell Medya"
                          width="149"
                        />
                      </figure>
                    </a>
                  </div>
                  <div className="swiper-slide swiper-lazy">
                    <a href="" title="Turkcell Partner Network">
                      <figure>
                        <img
                          src={CarouselLogo5}
                          alt="Turkcell Partner Network"
                          width="212"
                        />
                      </figure>
                    </a>
                  </div>
                  <div className="swiper-slide swiper-lazy">
                    <a href="" title="Lifecell">
                      <figure>
                        <img src={CarouselLogo6} alt="Lifecell" width="97" />
                      </figure>
                    </a>
                  </div>
                  <div className="swiper-slide swiper-lazy">
                    <a href="" title="Fizy">
                      <figure>
                        <img src={CarouselLogo7} alt="Fizy" width="39" />
                      </figure>
                    </a>
                  </div>
                  <div className="swiper-slide swiper-lazy">
                    <a href="" title="Upcall">
                      <figure>
                        <img src={CarouselLogo8} alt="Upcall" width="39" />
                      </figure>
                    </a>
                  </div>
                  <div className="swiper-slide swiper-lazy">
                    <a href="" title="Tv Plus">
                      <figure>
                        <img src={CarouselLogo9} alt="Tv Plus" width="39" />
                      </figure>
                    </a>
                  </div>
                  <div className="swiper-slide swiper-lazy">
                    <a href="" title="Paycell">
                      <figure>
                        <img src={CarouselLogo10} alt="Paycell" width="104" />
                      </figure>
                    </a>
                  </div>
                  <div className="swiper-slide swiper-lazy">
                    <a href="" title="Bip">
                      <figure>
                        <img src={CarouselLogo11} alt="Bip" width="39" />
                      </figure>
                    </a>
                  </div>
                  <div className="swiper-slide swiper-lazy">
                    <a href="" title="Gnç">
                      <figure>
                        <img src={CarouselLogo12} alt="Gnç" width="39" />
                      </figure>
                    </a>
                  </div>
                  <div className="swiper-slide swiper-lazy">
                    <a href="" title="Turkcell Superonline">
                      <figure>
                        <img
                          src={CarouselLogo13}
                          alt="Turkcell Superonline"
                          width="118"
                        />
                      </figure>
                    </a>
                  </div>
                </div>
              </div>
              <div className="container m-slider__container">
                <a
                  className="a-btn-icon m-slider__prev a-btn-icon--circle"
                  href="javascript:;"
                  title="Geri"
                >
                  <i className="icon-arrow-left" />
                  Geri
                </a>
                <a
                  className="a-btn-icon m-slider__next a-btn-icon--circle"
                  href="javascript:;"
                  title="İleri"
                >
                  <i className="icon-arrow-right" />
                  İleri
                </a>
              </div>
            </div>
          </div>
        </div>
        <hr className="o-footer__line" />
        <div className="container">
          <div className="o-footer__copy">
            <div className="m-grid">
              <div className="m-grid-col">
                <ul>
                  <li>
                    <a href="" title="Gizlilik ve güvenlik">
                      Gizlilik ve güvenlik
                    </a>
                  </li>
                  <li>
                    <a href="" title="Site haritası">
                      Site haritası
                    </a>
                  </li>
                  <li>
                    <a href="" title="Tarife karşılaştırma">
                      Tarife karşılaştırma
                    </a>
                  </li>
                </ul>
              </div>
              <div className="m-grid-col-2">
                <span className="o-footer__copyright">© 2019 Turkcell</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </footer>
  )
}

export default Footer

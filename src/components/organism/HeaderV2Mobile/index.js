/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"

import Logo from "../../../assets/images/content/turkcell-logo.png"

const HeaderV2Mobile = () => {
  return (
    <section id="header">
      <header className="o-header-v2">
        <div className="a-page-back">
          <a href="#" className="color_white">
            <i className="a-icon icon-arrow-left color_white" />
            Geri Dön
          </a>
        </div>
        <div className="o-header-v2__logo">
          <a className="m-logo" href="/turkcell.com.tr">
            <img src={Logo} alt="Turkcell" />
          </a>
        </div>
      </header>
    </section>
  )
}

export default HeaderV2Mobile

/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable no-script-url */
import React from "react"
import { HeaderDropdown } from "components/molecules"
import Logo from "assets/images/content/turkcell-logo.png"

const HeaderV2 = () => (
  <header className="o-header o-header--not-bottom o-header--pinned o-header--top">
    <div className="container">
      <div className="o-header__bar">
        <div className="o-header__logo">
          <a className="m-logo" href="/turkcell.com.tr">
            <img src={Logo} alt="Turkcell" />
          </a>
        </div>
        <nav className="m-header-nav">
          <a href="javascript:;" data-index="0" title="Cihazlar" className="">
            Cihazlar
          </a>
          <a href="javascript:;" data-index="1" title="Paketler" className="">
            Paketler
          </a>
          <a
            href="javascript:;"
            data-index="2"
            title="Uygulamalar"
            className=""
          >
            Uygulamalar
          </a>
          <a href="javascript:;" data-index="3" title="Ev Çözümleri">
            Ev Çözümleri
          </a>
          <a href="javascript:;" data-index="4" title="Fırsatlar">
            Fırsatlar
          </a>
        </nav>
        <div className="o-header__right">
          <HeaderDropdown />
          <div className="o-header__buttons">
            <div className="m-btn-group m-btn-group--align-right">
              <a
                className="a-btn-icon js-search"
                aria-label="Arama yapın"
                href="javascript:;"
                title="Arama Yapın"
                role="button"
              >
                <i className="icon-search" />
              </a>
              <div className="a-user-basket">
                <a
                  className="a-btn-icon js-basket"
                  aria-label="Sepetiniz"
                  href="javascript:;"
                  title="Sepetiniz"
                  role="button"
                >
                  <i className="icon-cart-notification" />
                </a>
              </div>
              <a
                className="a-btn-icon js-login"
                aria-label="Giriş Yapın"
                href="javascript:;"
                title="Giriş Yapın"
                role="button"
              >
                <i className="icon-account-regular" />
              </a>

              <div className="o-header--top_link">
                <a aria-label="Kurumsal" href="javascript:;" title="Kurumsal">
                  Kurumsal
                  <i className="a-icon icon-arrow-left rotate" />
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
)

export default HeaderV2

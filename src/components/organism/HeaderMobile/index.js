/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"
import Logo from "assets/images/content/turkcell-logo.png"

const HeaderMobile = () => (
  <header className="o-header-mobile" data-component="MobileHeader">
    <div className="container">
      <div className="o-header-mobile__bar">
        <div className="o-header-mobile__logo">
          <a className="m-logo" href="/turkcell.com.tr">
            <img src={Logo} alt="Turkcell" />
          </a>
        </div>
        <div className="o-header-mobile__right">
          <div className="o-header-mobile__buttons">
            <div className="m-btn-group m-btn-group--align-right">
              <a
                className="a-btn-icon js-search"
                aria-label="Arama Yapın"
                href="javascript:;"
                title="Arama Yapın"
                role="button"
              >
                <i className="icon-search" />
              </a>
              <div className="a-user-basket">
                <a
                  className="a-btn-icon js-basket"
                  aria-label="Sepetiniz"
                  href="javascript:;"
                  title="Sepetiniz"
                  role="button"
                >
                  <i className="icon-cart-notification" />
                </a>
                <div className="a-user-basket__badge">3</div>
              </div>
              <a
                className="a-btn-icon js-login"
                aria-label="Giriş Yapın"
                href="javascript:;"
                title="Giriş Yapın"
                role="button"
              >
                <i className="icon-account-regular" />
              </a>
              <a
                className="a-btn-icon js-mobile-nav"
                aria-label="Menü"
                href="javascript:;"
                title="Menü"
                role="button"
              >
                <i className="icon-mobile-menu" />
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </header>
)
export default HeaderMobile

import Header from "./Header"
import HeaderV2 from "./HeaderV2"
import Footer from "./Footer"

import HeaderMobile from "./HeaderMobile"
import FooterMobile from "./FooterMobile"

import HeaderV2Mobile from "./HeaderV2Mobile"

export { Header, HeaderV2, HeaderV2Mobile, Footer, HeaderMobile, FooterMobile }

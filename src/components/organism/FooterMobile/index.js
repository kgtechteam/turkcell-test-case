/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"
import CarouselLogo1 from "../../../assets/images/dummy/akilli-bulut.png"
import CarouselLogo2 from "../../../assets/images/dummy/gelecegi-yazanlar.png"
import CarouselLogo3 from "../../../assets/images/dummy/tedarikci.png"
import CarouselLogo4 from "../../../assets/images/dummy/medya.png"
import CarouselLogo5 from "../../../assets/images/dummy/partner-network.png"

import "./index.scss"

const FooterMobile = () => (
  <footer className="o-footer" data-component="MobileNavFooter">
    <div className="o-footer-top">
      <div className="m-grid">
        <div className="m-grid-col">
          <div className="a-mbreadcrumb" aria-label="Breadcrumb">
            <div
              className="m-slider"
              data-component='{"slidesPerView":"auto","spaceBetween":16,"centeredSlides":false,"type":"Slider"}'
            >
              <div className="m-slider__swiper swiper-container">
                <div className="swiper-wrapper">
                  <div className="swiper-slide swiper-lazy">
                    <a href="javascript:;" title="Ana Sayfa">
                      Ana Sayfa
                    </a>
                  </div>
                  <div className="swiper-slide swiper-lazy">
                    <a href="javascript:;" title="Cihazlar">
                      Cihazlar
                    </a>
                  </div>
                  <div className="swiper-slide swiper-lazy">
                    <a href="javascript:;" title="Akıllı Telefonlar">
                      Akıllı Telefonlar
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="m-grid-col">
          <nav className="o-footer-nav" data-component="MobileNavFooter">
            <ul>
              <li className="dropdown">
                <a href="javascript:;" title="Hakkımızda">
                  Hakkımızda
                </a>
              </li>
              <li className="dropdown">
                <a href="javascript:;" title="Popüler Linkler">
                  Popüler Linkler
                </a>
                <ul>
                  <li>
                    <a href="javascript:;" title="Cihazlar">
                      Cihazlar
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Paketler">
                      Paketler
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Dijital Servisler">
                      Dijital Servisler
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Ev Çözümleri">
                      Ev Çözümleri
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Engel Tanımayanlar">
                      Engel Tanımayanlar
                    </a>
                  </li>
                </ul>
              </li>
              <li className="dropdown">
                <a href="javascript:;" title="Müşteri Hizmetleri">
                  Müşteri Hizmetleri
                </a>
                <ul>
                  <li>
                    <a href="javascript:;" title="Cihaz Ayar Desteği">
                      Cihaz Ayar Desteği
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Sipariş Takibi">
                      Sipariş Takibi
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="İade Talebi">
                      İade Talebi
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Puk Sorgulama">
                      Puk Sorgulama
                    </a>
                  </li>
                  <li>
                    <a
                      href="javascript:;"
                      title="Alacak Sorgulama / TL İade Talep"
                    >
                      Alacak Sorgulama / TL İade Talep
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Şikayet Talebi Oluşturma">
                      Şikayet Talebi Oluşturma
                    </a>
                  </li>
                  <li>
                    <a href="javascript:;" title="Şikayet Takibi">
                      Şikayet Takibi
                    </a>
                  </li>
                </ul>
              </li>
              <li>
                <a
                  href="javascript:;"
                  className="arrow-right"
                  title="Size En Yakın Mağaza"
                >
                  Size En Yakın Mağaza
                </a>
              </li>
            </ul>
          </nav>
        </div>
        <div className="m-grid-col">
          <div className="o-footer__social">
            <strong>Bizi Takip Edin</strong>
            <div className="m-btn-group">
              <a className="a-btn-icon" href="javascript:;" title="Twitter">
                <i className="icon-twitter" />
              </a>
              <a className="a-btn-icon" href="javascript:;" title="Facebook">
                <i className="icon-facebook" />
              </a>
              <a className="a-btn-icon" href="javascript:;" title="İnstagram">
                <i className="icon-instagram" />
              </a>
              <a className="a-btn-icon" href="javascript:;" title="Youtube">
                <i className="icon-youtube" />
              </a>
              <a className="a-btn-icon" href="javascript:;" title="LinkedIn">
                <i className="icon-linkedin" />
              </a>
            </div>
          </div>
        </div>
        <div className="m-grid-col">
          <div className="mobile__language">
            <strong>Language</strong>
            <div
              className="m-dropdown m-dropdown--clickable"
              data-component="Dropdown"
              role="navigation"
            >
              <input type="checkbox" id="s0-0-31-2-19-dropdown" />
              <label
                aria-haspopup="true"
                title="Türkçe"
                htmlFor="s0-0-31-2-19-dropdown"
              >
                Türkçe
              </label>
              <ul aria-hidden="true" aria-labelledby="s0-0-31-2-19-dropdown">
                <li>
                  <a href="javascript:;" title="English">
                    English
                  </a>
                </li>
                <li>
                  <a href="javascript:;" title="Türkçe">
                    Türkçe
                  </a>
                </li>
                <li>
                  <a href="javascript:;" title="عربى">
                    عربى
                  </a>
                </li>
                <li>
                  <a href="javascript:;" title="русский">
                    русский
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div className="o-footer-bottom">
      <div className="o-footer__partners">
        <div
          className="m-slider"
          data-component='{"slidesPerView":"auto","spaceBetween":32,"type":"Slider"}'
        >
          <div className="m-slider__swiper swiper-container">
            <div className="swiper-wrapper">
              <div className="swiper-slide swiper-lazy">
                <a href="" title="Turkcell Akıllı Bulut">
                  <figure>
                    <img
                      src={CarouselLogo1}
                      alt="Turkcell Akıllı Bulut"
                      width="171"
                    />
                  </figure>
                </a>
              </div>
              <div className="swiper-slide swiper-lazy">
                <a href="" title="Turkcell Geleceği Yazanlar">
                  <figure>
                    <img
                      src={CarouselLogo2}
                      alt="Turkcell Geleceği Yazanlar"
                      width="222"
                    />
                  </figure>
                </a>
              </div>
              <div className="swiper-slide swiper-lazy">
                <a href="" title="Turkcell Tedarikçi">
                  <figure>
                    <img
                      src={CarouselLogo3}
                      alt="Turkcell Tedarikçi"
                      width="212"
                    />
                  </figure>
                </a>
              </div>
              <div className="swiper-slide swiper-lazy">
                <a href="" title="Turkcell Medya">
                  <figure>
                    <img src={CarouselLogo4} alt="Turkcell Medya" width="149" />
                  </figure>
                </a>
              </div>
              <div className="swiper-slide swiper-lazy">
                <a href="" title="Turkcell Partner Network">
                  <figure>
                    <img
                      src={CarouselLogo5}
                      alt="Turkcell Partner Network"
                      width="212"
                    />
                  </figure>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="o-footer__copy">
        <div className="m-grid">
          <div className="m-grid-col">
            <ul>
              <li>
                <a href="" title="Gizlilik ve güvenlik">
                  Gizlilik ve güvenlik
                </a>
              </li>
              <li>
                <a href="" title="Site haritası">
                  Site haritası
                </a>
              </li>
              <li>
                <a href="" title="Tarife karşılaştırma">
                  Tarife karşılaştırma
                </a>
              </li>
            </ul>
          </div>
          <div className="m-grid-col">
            <span className="o-footer__copyright">© 2019 Turkcell</span>
          </div>
        </div>
      </div>
    </div>
  </footer>
)

export default FooterMobile

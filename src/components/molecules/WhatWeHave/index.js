/* eslint-disable react/no-unescaped-entities */
import React from "react"

const WhatWeHave = () => (
  <div className="m-what-we-have">
    <div className="container">
      <h2 className="compact-title">Turkcell'de ne var?</h2>
      <ul>
        <li>
          <span className="a-btn-icon a-btn-icon--medium-b a-btn-icon--circle">
            <i className="icon-warranty" />
            ButtonIcon
          </span>{" "}
          Yenilikçi Hizmet
        </li>
        <li>
          <span className="a-btn-icon a-btn-icon--medium-b a-btn-icon--circle">
            <i className="icon-credit-card" />
            ButtonIcon
          </span>{" "}
          Ertesi gün teslimat
        </li>
        <li>
          <span className="a-btn-icon a-btn-icon--medium-b a-btn-icon--circle">
            <i className="icon-return" />
            ButtonIcon
          </span>{" "}
          7/24 Müşteri hizmetleri
        </li>
        <li>
          <span className="a-btn-icon a-btn-icon--medium-b a-btn-icon--circle">
            <i className="icon-turkcell-logo" />
            ButtonIcon
          </span>{" "}
          En hızlı internet bağlantısı
        </li>
      </ul>
    </div>
  </div>
)

export default WhatWeHave

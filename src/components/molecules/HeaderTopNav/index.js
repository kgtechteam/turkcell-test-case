/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"

const HeaderTopNav = () => (
  <div className="m-header-topnav">
    <div className="container">
      <ul>
        <li className="active">
          <a href="javascript:;" title="Bireysel">
            Bireysel
          </a>
        </li>
        <li>
          <a href="javascript:;" title="Kurumsal">
            Kurumsal
          </a>
        </li>
      </ul>
    </div>
  </div>
)

export default HeaderTopNav

import React from "react"

import CarouselLogo1 from "../../../assets/images/dummy/bip-icon@2x.png"
import CarouselLogo2 from "../../../assets/images/dummy/fizy-icon@2x.png"
import CarouselLogo3 from "../../../assets/images/dummy/dergilik-icon@2x.png"
import CarouselLogo4 from "../../../assets/images/dummy/yaani-icon@2x.png"
import CarouselLogo5 from "../../../assets/images/dummy/gnc-icon@2x.png"
import CarouselLogo6 from "../../../assets/images/dummy/upcall-icon@2x.png"
import CarouselLogo7 from "../../../assets/images/dummy/fulltrip-icon@2x.png"
import CarouselLogo8 from "../../../assets/images/dummy/paycell-icon@2x.png"

const ProductDetailPackages = () => {
  return (
    <div
      className="m-slider m-slider__tabs m-slider__tabs--no-space m-slider__tabs--iconless"
      data-component='{"slidesPerView":8,"spaceBetween":10,"navigation":true,"type":"Slider"}'
    >
      <div className="m-slider__swiper swiper-container with-border">
        <div className="swiper-wrapper">
          <div className="swiper-slide swiper-lazy active">
            <a href="" title="Tümü" role="tab">
              <img width="60px" src={CarouselLogo1} alt="Tümü" />
            </a>
          </div>
          <div className="swiper-slide swiper-lazy">
            <a href="javascript:;" title="Cihazlar" role="tab">
              <img width="60px" src={CarouselLogo2} alt="Cihazlar" />
            </a>
          </div>
          <div className="swiper-slide swiper-lazy">
            <a href="javascript:;" title="Paketler" role="tab">
              <img width="60px" src={CarouselLogo3} alt="Paketler" />
            </a>
          </div>
          <div className="swiper-slide swiper-lazy">
            <a href="javascript:;" title="Uygulamalar" role="tab">
              <img width="60px" src={CarouselLogo4} alt="Uygulamalar" />
            </a>
          </div>
          <div className="swiper-slide swiper-lazy">
            <a href="javascript:;" title="Fırsatlar" role="tab">
              <img width="60px" src={CarouselLogo5} alt="Fırsatlar" />
            </a>
          </div>
          <div className="swiper-slide swiper-lazy">
            <a href="javascript:;" title="Fırsatlar" role="tab">
              <img width="60px" src={CarouselLogo6} alt="Fırsatlar" />
            </a>
          </div>
          <div className="swiper-slide swiper-lazy">
            <a href="javascript:;" title="Fırsatlar" role="tab">
              <img width="60px" src={CarouselLogo7} alt="Fırsatlar" />
            </a>
          </div>
          <div className="swiper-slide swiper-lazy">
            <a href="javascript:;" title="Fırsatlar" role="tab">
              <img width="60px" src={CarouselLogo8} alt="Fırsatlar" />
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProductDetailPackages

import React from "react"

import Benefits1 from "../../../assets/images/dummy/bip@2x.png"
import Benefits2 from "../../../assets/images/dummy/fizy@2x.png"
import Benefits3 from "../../../assets/images/dummy/abroad@2x.png"
import Benefits4 from "../../../assets/images/dummy/lifebox@2x.png"
import Benefits5 from "../../../assets/images/dummy/tv@2x.png"

const ProductDetailBenefits = () => {
  return (
    <div>
      <div className="benefits-list-item">
        <img className="m-r-20" src={Benefits1} alt="" />
        <span>Bip’e özel ek 5 GB ile yüksek kaliteli görüntülü konuşun.</span>
      </div>

      <div className="benefits-list-item">
        <img className="m-r-20" src={Benefits2} alt="" />
        <span>
          TV+’ta geçerli 5 GB internet ile canlı TV kanalları ve sınırsız sayıda
          seç izle film kataloğu sizinle.
        </span>
      </div>

      <div className="benefits-list-item">
        <img className="m-r-20" src={Benefits3} alt="" />
        <span>
          Fizy’e özel 5 GB internet ile müzik ve videonun keyfini çıkarın.
        </span>
      </div>

      <div className="benefits-list-item">
        <img className="m-r-20" src={Benefits4} alt="" />
        <span>
          Lifebox’a özel aylık 5 GB internet ile anılarınızı 25 GB depolama
          alanında güvenle saklayıp, Premium özelliklerin keyfini çıkarın.
        </span>
      </div>

      <div className="benefits-list-item">
        <img className="m-r-20" src={Benefits5} alt="" />
        <span>
          Dergilik ile Türkiye’nin popüler yüzlerce dergi ve gazetesini keyifle
          okuyun.
        </span>
      </div>
    </div>
  )
}

export default ProductDetailBenefits

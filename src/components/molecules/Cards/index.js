/* eslint-disable import/no-dynamic-require */
/* eslint-disable react/prop-types */
/* eslint-disable global-require */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"

const Card = ({ packageItem }) => (
  <div style={{ textAlign: "center" }}>
    <div className="inline-block">
      <a href="#" className="m-card m-card--package">
        <div
          className="m-card__head__short-hat"
          style={{
            background: packageItem.color
          }}
        />
        <div
          className="m-card__head"
          style={{
            background: packageItem.color,
            color: "#ffffff"
          }}
        >
          <h3 className="m-card__title">{packageItem.name}</h3>
        </div>
        <div className="m-card__body">
          <div className="m-card__packages">
            {packageItem.services.map(service => (
              <img
                src={require(`../../../assets/images/dummy/${service}@2x.png`)}
                alt={service}
              />
            ))}
          </div>
          <div className="m-card__tariffs">
            <div className="a-tariff">
              <i className="a-icon icon-cellular" />
              <div className="a-tariff__data">
                {packageItem.internet.main}
                <sup>GB</sup>
              </div>
              <div className="a-tariff__name">İnternet</div>
            </div>
            <div className="a-tariff">
              <i className="a-icon icon-telephone" />
              <div className="a-tariff__data">
                {packageItem.call}
                <sup>DK</sup>
              </div>
              <div className="a-tariff__name">Her Yöne</div>
            </div>
          </div>
        </div>
        <div className="m-card__foot">
          <div className="m-flex">
            <span className="a-price a-price--gray">
              {packageItem.price}
              <sup>
                <span className="a-price__currency">TL</span>/AY
              </sup>
            </span>
          </div>
        </div>
      </a>
    </div>
  </div>
)

export default Card

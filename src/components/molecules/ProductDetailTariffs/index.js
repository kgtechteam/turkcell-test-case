import React from "react"

const ProductDetailTariffs = props => {
  return (
    <div className={`m-flex ${props.className}`}>
      <div className="a-tariff a-tariff--bordered large-box">
        <i className="a-icon icon-cellular" />
        <div className="a-tariff__data m-l-20">
          50<sup>GB</sup>
        </div>
        <div className="a-tariff__name m-t-10">İnternet</div>
      </div>

      <div className="a-tariff a-tariff--bordered m-grid-col-3">
        <i className="a-icon icon-telephone" />
        <div className="a-tariff__data">
          2000<sup>DK</sup>
        </div>
        <div className="a-tariff__name">Yurtiçi Arama</div>
      </div>

      <div className="a-tariff a-tariff--bordered m-grid-col-3">
        <i className="a-icon icon-telephone" />
        <div className="a-tariff__data">
          2000<sup>DK</sup>
        </div>
        <div className="a-tariff__name">Yurtiçi Arama</div>
      </div>
    </div>
  )
}

export default ProductDetailTariffs

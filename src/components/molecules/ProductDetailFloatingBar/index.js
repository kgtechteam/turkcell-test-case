/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"

import floatingBarImage from "../../../assets/images/dummy/floatingBar-image.png"

export const ProductDetailFloatingBar = () => {
  return (
    <div id="product-detail-floating-bar" className="m-b-70">
      <img className="w-100" alt="" src={floatingBarImage} />

      <div className="container">
        <div
          className="m-card m-card--package m-card--package--no-package w-100"
          style={{ marginTop: -150 }}
        >
          <div className="m-card__body">
            <div className="m-card__info text-center">
              <h3 className="block text-center m-b-20">
                Floating Bar ile Kolay Erişim
              </h3>
              <span className="text-center block">
                LG's revolutionary OLED display incorporates plastic for higher
                flexibility.
              </span>

              <a
                className="a-btn a-btn--with-icon icon-plus a-btn--with-icon--start a-btn--secondary m-t-30 inline-block"
                href="javascript:;"
              >
                Daha Fazla
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default ProductDetailFloatingBar

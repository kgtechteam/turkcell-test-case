import React from "react"

import ProductVideo from "../../../assets/images/dummy/product-video.jpg"

const ProductDetailVideo = () => {
  return (
    <section id="product-social-video" className="white">
      <div className="container">
        <div className="m-grid">
          <div className="m-grid-col">
            <div
              className="m-product-video-banner lazyloaded"
              data-background={ProductVideo}
              style={{ backgroundImage: `url(${ProductVideo})` }}
            >
              <a
                className="a-btn-icon a-btn-icon--big a-btn-icon--circle a-btn-icon--circle--yellow"
                data-fancybox=""
                data-base-class="fancybox-video-mobile"
                href="https://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4"
              >
                <i className="icon-play" />
              </a>
              <p>LG V30+ Deneyimini Keşfet</p>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default ProductDetailVideo

/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/label-has-associated-control */
import React from "react"

const HeaderDropdown = () => {
  return (
    <div className="m-dropdown js-quick-nav" role="navigation">
      <input type="checkbox" id="s0-0-0-15-10-dropdown" />
      <label
        aria-haspopup="true"
        title="Hızlı İşlemler"
        htmlFor="s0-0-0-15-10-dropdown"
      >
        Hızlı İşlemler
      </label>
      <ul aria-hidden="true" aria-labelledby="s0-0-0-15-10-dropdown">
        <li>
          <a href="javascript:;" title="TL Yükle">
            TL Yükle
          </a>
        </li>
        <li>
          <a href="javascript:;" title="Fatura Sorgula / Öde">
            Fatura Sorgula / Öde
          </a>
        </li>
        <li>
          <a href="javascript:;" title="Hazır Kart Paket Yükle">
            Hazır Kart Paket Yükle
          </a>
        </li>
        <li>
          <a href="javascript:;" title="Numara Taşıma">
            Numara Taşıma
          </a>
        </li>
        <li>
          <a href="javascript:;" title="Yeni Hat Alma">
            Yeni Hat Alma
          </a>
        </li>
      </ul>
    </div>
  )
}

export default HeaderDropdown

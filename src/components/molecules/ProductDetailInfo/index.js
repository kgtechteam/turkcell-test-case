import React from "react"

import ProductDetailTariffs from "../ProductDetailTariffs"
import ProductDetailPackages from "../ProductDetailPackages"
import ProductDetailCheckbox from "../ProductDetailCheckbox"

const ProductDetailInfo = () => {
  return (
    <section id="product-detail" className="product-detail">
      <div className="container m-t-30">
        <div className="product-detail-top">
          <div className="m-grid">
            <div className="m-grid-col-6 p-r-100">
              <div className="a-ribbon--container">
                <div className="a-ribbon a-ribbon--b">Dijitale Özel</div>
              </div>

              <ProductDetailTariffs />

              <span className="m-t-50 block">
                <strong>Turkcell Uygulama Dünyası Paket Kapsamı</strong>
              </span>

              <ProductDetailPackages />
            </div>

            <div className="m-grid-col-6">
              <h1>Platinum + Maksi Paketi</h1>
              <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. Etiam
                ornare tellus ut arcu lacinia tempor. Curabitur ullamcorper
                finibus velit. Fusce aliquet metus non ipsum.
              </p>

              <ProductDetailCheckbox />

              <div className="m-btn-group">
                <a className="a-btn a-btn--full" href="javascript:;">
                  Hemen Al
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default ProductDetailInfo

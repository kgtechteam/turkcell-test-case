import React from "react"

import Image1 from "../../../assets/images/dummy/1.png"
import Image2 from "../../../assets/images/dummy/2.png"
import Image3 from "../../../assets/images/dummy/3.png"
import Image4 from "../../../assets/images/dummy/4.png"

import productVideoImage from "../../../assets/images/dummy/M_PC_ProductVideo.png"

const ProductDetailMoreInfoMobile = () => {
  return (
    <section id="product-detail-info" className="white">
      <div
        className="m-product-detail-info p-b-40 border-bottom-0"
        data-component='{"lessText":"Daha Az","scrollToTop":true,"type":"MoreContent"}'
      >
        <div className="js-more-container">
          <article className="js-more-content__first">
            <div className="container">
              <div className="clearfix m-flex justify-content-center align-items-center">
                <div className="text-left">
                  <h2>Tarifeniz Yurt Dışında Geçerli</h2>
                  <p className="w-100">
                    Paketinizin geçerli olduğu ülkeler: Almanya, Hollanda,
                    İngiltere, Rusya, Hindistan
                    <br />
                    Paketinizi kullanma koşulunuz: Paketinizin özelliği gereği,
                    yurt dışında geçerli ülkelerde, yurt içi tarifenizdeki her
                    yöne dakikalarınızla bulunduğunuz ülke ve Türkiye'yi
                    arayabilir, internet ve SMS'lerinizi otomatik olarak günlük
                    ek ücret karşılığında kullanabilirsiniz.
                  </p>
                </div>
              </div>
            </div>
            <img
              className="w-100 block rounded-none"
              alt=""
              src={productVideoImage}
            />
          </article>

          <div className="js-more-content__more">
            <article>
              <div className="container">
                <div className="clearfix">
                  <div className="visual pull-right">
                    <img src={Image1} alt="" />
                  </div>
                  <h2>Yeni Huawei FullView Display Ekran</h2>
                  <ul>
                    <li>
                      Daha fazla hassasiyet için dokunsal yaklaşımı kullanın.
                    </li>
                    <li>
                      Kamera ayarlarını değiştirebilir ve çoklu duyusal deneyimi
                      yaşatan bildirimler.
                    </li>
                    <li>
                      Hi-Fi Quad DAC kayıttan yürütme özelliği, canlıda olduğu
                      gibi zengin.
                    </li>
                    <li>
                      Duygusal Dokunsal geri bildirim ile satın alımlar
                      yapabilir, hassas kamera ayarlarını değiştirebilir ve
                      çoklu duyusal deneyimi yaşatan bildirimler alabilirsiniz.
                    </li>
                  </ul>
                </div>
              </div>
            </article>
            <article>
              <div className="container">
                <div className="clearfix">
                  <div className="visual pull-left">
                    <img src={Image2} alt="" />
                  </div>
                  <h2>Portre Modunda Selfie</h2>
                  <ol>
                    <li>
                      Daha fazla hassasiyet için dokunsal yaklaşımı kullanın.
                    </li>
                    <li>
                      Kamera ayarlarını değiştirebilir ve çoklu duyusal deneyimi
                      yaşatan bildirimler.
                    </li>
                    <li>
                      Hi-Fi Quad DAC kayıttan yürütme özelliği, canlıda olduğu
                      gibi zengin.
                    </li>
                    <li>
                      Duygusal Dokunsal geri bildirim ile satın alımlar
                      yapabilir, hassas kamera ayarlarını değiştirebilir ve
                      çoklu duyusal deneyimi yaşatan bildirimler alabilirsiniz.
                    </li>
                  </ol>
                  <p>
                    2 MP profesyonel Bokeh merceği ile birlikte konu ve arka
                    plan için 5P + 3P lensler ile birlikte karışıma bazı gerçek
                    tanımları getirirken çarpıcı çekimler yapmanıza imkan
                    sağlıyor.
                  </p>
                </div>
              </div>
            </article>
            <article>
              <div className="container">
                <div className="clearfix">
                  <div className="visual pull-right">
                    <img src={Image3} alt="" />
                  </div>
                  <h2>Yeni&nbsp;Çift Kamera</h2>
                  <p>
                    Huawei P20 Lite'nin çift objektifli arka kamerası sizi
                    bekliyor. Piksel boyutu 1,12 μm ve F2.2 diyaframa sahip son
                    teknoloji ürünü 16 MP objektif, daha fazla ışık ve okuma
                    derinliği algısı elde etmek için tasarlanmıştır ve ihtiyaç
                    duyduğunuzda hızlı bir şekilde çekim yapmanızı sağlar. 2 MP
                    profesyonel Bokeh merceği ile birlikte konu ve arka plan
                    için 5P + 3P lensler ile birlikte karışıma bazı gerçek
                    tanımları getirirken çarpıcı çekimler yapmanıza imkan
                    sağlıyor.
                  </p>
                </div>
              </div>
            </article>
            <article>
              <div className="container">
                <div className="clearfix">
                  <div className="visual pull-left">
                    <img src={Image4} alt="" />
                  </div>
                  <h2>Üstün Yapay Zeka</h2>
                  <p>
                    Huawei'in yeni EMUI 8.0 modeli, Huawei P20 Lite cihazının
                    daha hızlı ve kolay çalışmasını sağlayarak hız ve
                    performansı optimize etmek için yapay zeka kullanan bir
                    arayüze sahiptir.
                  </p>
                  <table
                    cellSpacing="0"
                    width="100%"
                    className="singlehub-rteTable-default"
                  >
                    <tbody>
                      <tr>
                        <td
                          className="singlehub-rteTable-default"
                          style={{ width: "33.3333%;" }}
                        >
                          Paket
                        </td>
                        <td
                          className="singlehub-rteTable-default"
                          style={{ width: "33.3333%;" }}
                        >
                          İçerik
                        </td>
                        <td
                          className="singlehub-rteTable-default"
                          style={{ width: "33.3333%;" }}
                        >
                          Ücret
                        </td>
                      </tr>
                      <tr>
                        <td className="singlehub-rteTable-default">
                          Yurt Dışı Yönüne Arama Paketi
                        </td>
                        <td className="singlehub-rteTable-default">
                          60 dakika
                        </td>
                        <td className="singlehub-rteTable-default">14 TL</td>
                      </tr>
                      <tr>
                        <td className="singlehub-rteTable-default">
                          Yurt Dışı Yönüne SMS paketi
                        </td>
                        <td className="singlehub-rteTable-default">30 SMS</td>
                        <td className="singlehub-rteTable-default">3 TL</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
              </div>
            </article>
            <article>
              <div className="container">
                <div className="clearfix">
                  <div className="clearfix">
                    <div className="no-image">
                      <h2>Özellikler</h2>
                      <p />
                      <ul>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Katlanabilir, Gürültü Engelleyici Mikrofon
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>Yu</span>
                          <span style={{ lineHeight: "1.6;" }}>
                            muşak ve delikli malzemeler
                          </span>
                          <br />
                        </li>
                      </ul>
                      <br />
                      <strong>Sistem Gereksinimleri</strong>
                      <br />
                      <ul>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Windows 10, Windows 8.1, Windows 8, Windows 7, PS4
                            veya Xbox One
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            USB bağlantı noktası, 3.5 mm ses bağlantı noktası
                            veya RCA ses çıkışı bağlantı noktaları
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Logitech Gaming Software yüklemesi için İnternet
                            bağlantısı gereklidir
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            7.1 surround ses, aydınlatma yapılandırması ve G
                            tuşu programlama özellikleri için Logitech Gaming
                            Software ve USB bağlantı nokt
                          </span>
                          <span style={{ lineHeight: "1.6;" }}>
                            ası gereklidir. (Yazılımı
                            gaming.logitech.com/support adresinden
                            indirebilirsiniz).
                          </span>
                          <br />
                        </li>
                      </ul>
                      <br />
                      <strong>Paket İçeriği</strong>
                      <br />
                      <ul>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Performans Tipi Oyun Kulaklığı
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Ses denetimlerine sahip 3.5 mm analog kablo
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            RCA/3.5 mm kablo
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            USB veri ve şarj kablosu
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            USB’den çalışan kablosuz karıştırma adaptörü
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Özel etiketler (sol ve sağ)
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>Kull</span>
                          <span style={{ lineHeight: "1.6;" }}>
                            anıcı belgeleri
                          </span>
                          <br />
                        </li>
                      </ul>
                      <br />
                      <strong>Kulaklık</strong>
                      <br />
                      <ul>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Sürücü: 40 mm
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Frekans yanıtı: 20 Hz – 20 KHz
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Empedans: 39 Ohm (pasif), 5000 Ohm (aktif)
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>Hass</span>
                          <span style={{ lineHeight: "1.6;" }}>
                            asiyet: 107 dB SPL/mW
                          </span>
                          <br />
                        </li>
                      </ul>
                      <br />
                      <b>Mikrofon (Kol)</b>
                      <br />
                      <ul>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Mikrofon Toplama düzeni: Kardiyot (Tek yönlü)
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Boyut: 4 mm
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>Fre</span>
                          <span style={{ lineHeight: "1.6;" }}>
                            kans yanıtı: 100 Hz – 20 KHz
                          </span>
                          <br />
                        </li>
                      </ul>
                      <br />
                      <strong>Kablosuz Menzil</strong>
                      <br />
                      <ul>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            İç mekan: 15 metre
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Dış mekan: 20 metre
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>(Ku</span>
                          <span style={{ lineHeight: "1.6;" }}>
                            llanım mesafesi, çevresel kullanım koşullarına bağlı
                            olarak değişiklik gösterebilir)
                          </span>
                          <br />
                        </li>
                      </ul>
                      <br />
                      <strong>Bağlantı</strong>
                      <br />
                      <ul>
                        <li>
                          <span style={{ lineHeight: "1.6;" }} />
                          <span style={{ lineHeight: "1.6;" }} />
                          <span style={{ lineHeight: "1.6;" }} />
                          <span style={{ lineHeight: "1.6;" }}>B</span>
                          <span style={{ lineHeight: "1.6;" }}>
                            ağlantı Türü: USB
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            LCD Ekran: Yok
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Pil Ömrü (yeniden şarj edilebilir):
                          </span>
                          <br />
                        </li>
                        <ul>
                          <li>
                            <span style={{ lineHeight: "1.6;" }}>
                              Aydınlatmas
                            </span>
                          </li>
                          <li>
                            <span style={{ lineHeight: "1.6;" }} />
                          </li>
                          <li>
                            <span style={{ lineHeight: "1.6;" }} />
                          </li>
                          <li>
                            <span style={{ lineHeight: "1.6;" }}>at</span>
                          </li>
                          <li>
                            <span style={{ lineHeight: "1.6;" }}>
                              Varsayılan aydınlatmayla: 8 saat
                            </span>
                          </li>
                          <li>
                            <span style={{ lineHeight: "1.6;" }}>
                              %50 ses düzeyinde test edilmiştir
                            </span>
                          </li>
                        </ul>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Ses Denetimleri: Var
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Mikrofon: Var
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Mikrofon Türü: Kardiyot (Tek yönlü)
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>
                            Gürültü Önleme: Var
                          </span>
                          <br />
                        </li>
                        <li>
                          <span style={{ lineHeight: "1.6;" }}>(Şarj</span>
                          <span style={{ lineHeight: "1.6;" }}>
                            {" "}
                            edilebilir pillerin, yeniden şarj edilebilme sayısı
                            sınırlıdır. Pil ömrü %50 ses düzeyinde
                            hesaplanmıştır)
                          </span>
                          <br />
                        </li>
                      </ul>
                    </div>
                    <p />
                  </div>
                </div>
              </div>
            </article>
          </div>
        </div>
        <div className="m-btn-group">
          <a
            className="a-btn a-btn--with-icon icon-plus a-btn--with-icon--start a-btn--secondary js-more-content__button"
            href="javascript:;"
          >
            Daha Fazla
          </a>
        </div>
      </div>
    </section>
  )
}

export default ProductDetailMoreInfoMobile

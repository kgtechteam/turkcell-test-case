import React from "react"

const ProductDetailAccordion = () => {
  return (
    <section id="product-accordion" className="m-t-100">
      <div className="container">
        <div
          className="m-accordion m-accordion--plus"
          data-component='{"showOnlyOne":false,"type":"Accordion"}'
        >
          <div className="m-accordion__container">
            <div className="m-accordion__head m-accordion__head--active">
              <i />
              <span>Lorem ipsum dolor sit amet.</span>
            </div>
            <div className="m-accordion__body">
              <p>
                Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do
                eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
                enim ad minim veniam, quis nostrud exercitation ullamco laboris
                nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor
                in reprehenderit in voluptate velit esse cillum dolore eu fugiat
                nulla pariatur. Excepteur sint occaecat cupidatat non proident,
                sunt in culpa qui officia deserunt mollit anim id est laborum.
              </p>
            </div>
          </div>
          <div className="m-accordion__container">
            <div className="m-accordion__head">
              <i />
              <span>Ut enim ad minim veniam</span>
            </div>
            <div className="m-accordion__body">
              <p>
                Excepteur sint lorem ipsum dolor sit amet, consectetur
                adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                dolore magna aliqua. Ut enim ad minim veniam, quis nostrud
                exercitation ullamco laboris nisi ut aliquip ex ea commodo
                consequat. Duis aute irure dolor in reprehenderit in voluptate
                velit esse cillum dolore eu fugiat nulla pariatur. Excepteur
                sint occaecat cupidatat non proident, sunt in culpa qui officia
                deserunt mollit anim id est laborum.
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default ProductDetailAccordion

/* eslint-disable jsx-a11y/media-has-caption */
/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"
import "./index.scss"

const CarouselMobile = () => (
  <section className="m-carousel m-t-10 m-b-30" data-component="Carousel">
    <svg width="0" height="0">
      <defs>
        <clipPath id="m-carousel__clip-path" clipPathUnits="objectBoundingBox">
          <path
            d="M0,0 L0,0.9174311926605505 Q0.5,1.0825688073394495 1,0.9174311926605505 L1,0 Z"
            strokeWidth="0"
          />
        </clipPath>
        <clipPath
          id="m-carousel__clip-path--mobile"
          clipPathUnits="objectBoundingBox"
        >
          <path
            d="M0,0 L0,0.9764705882352941 Q0.5,1.0235294117647058 1,0.9764705882352941 L1,0 Z"
            strokeWidth="0"
          />
        </clipPath>
      </defs>
    </svg>
    <div
      className="m-slider"
      data-component='{"navigation":true,"type":"Slider"}'
    >
      <div className="m-slider__swiper swiper-container">
        <div className="swiper-wrapper">
          <div
            data-background="https://s1.turkcell.com.tr/SiteAssets/Banner/html5/turkiyenin-uygulamalari-bannerV2/images/bg.jpg"
            className="swiper-slide swiper-lazy"
          >
            <video
              className="a-video-banner"
              data-component="VideoBanner"
              data-autoplay="true"
              src="https://s3.turkcell.com.tr/SiteAssets/Banner/html5/kurumsal-cati-banner-anasayfaV2/PTOT_TURKCELL_V2.mp4"
            />
            <div className="m-grid">
              <div className="m-grid-col-5">
                <div className="m-carousel__desc m-carousel__desc--text-shadow">
                  <h3>
                    Birbirinden avantajlı okula dönüş fırsatları
                    turkcell.com.tr’de!
                  </h3>
                  <div className="m-btn-group m-btn-group--align-left m-btn-group--m-align-center">
                    <a className="a-btn" title="İncele" href="javascript:;">
                      İncele
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            data-background="https://s1.turkcell.com.tr/SiteAssets/Banner/html5/turkiyenin-uygulamalari-bannerV2/images/bg.jpg"
            className="swiper-slide swiper-lazy"
            data-location-href="/servisler/supercam?place=banner-3"
          >
            <div className="m-grid">
              <div className="m-grid-col text-center">
                <canvas
                  className="a-canvas-banner"
                  width="700"
                  height="400"
                  id="trspor02"
                  data-autoplay="true"
                  data-library="turkcelltruygulamalartamsayfa700x400"
                  data-adobe-an="AdobeAnTurkiyeninUygulamalariV2"
                  data-composition-code="E5476D635B9CBA438842AE3A7E380B29"
                  data-script="https://s1.turkcell.com.tr/SiteAssets/Banner/html5/turkiyenin-uygulamalari-bannerV2/turkiyenin-uygulamalariV2.js"
                  data-component="CanvasBanner"
                />
              </div>
            </div>
          </div>
          <div
            data-background="../../../assetsv2/common/images/dummy/5g-hazir-banner-bg.jpg"
            className="swiper-slide swiper-lazy"
            data-swiper-autoplay="4000"
            data-location-href="/servisler/supercam?place=banner-3"
          >
            <div className="m-grid">
              <div className="m-grid-col-5">
                <div className="m-carousel__desc m-carousel__desc--text-shadow">
                  <h3 data-animation="slideInLeft">
                    DJI Mavic Pro 36 aya varan taksit imkanları ile Turkcell’de.
                  </h3>
                  <div className="m-btn-group m-btn-group--align-left m-btn-group--m-align-center">
                    <a className="a-btn" title="İncele" href="javascript:;">
                      İncele
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            data-background="https://s3.turkcell.com.tr/SiteAssets/Banner/html5/turksporu-anim-bannerV2/images/bg.jpg"
            className="swiper-slide swiper-lazy"
            data-location-href="/servisler/supercam?place=banner-3"
          >
            <div className="m-grid">
              <div className="m-grid-col-6">
                <div className="m-carousel__desc m-carousel__desc--text-shadow">
                  <h3>TURKSPORU Sahada</h3>
                  <p>
                    Türk sporuna dair her şey artık www.turksporu.com.tr’de!
                  </p>
                  <div className="m-btn-group m-btn-group--align-left">
                    <a className="a-btn" title="İncele" href="javascript:;">
                      İncele
                    </a>
                  </div>
                </div>
              </div>
              <div className="m-grid-col-6 text-right">
                <canvas
                  className="a-canvas-banner"
                  width="500"
                  height="400"
                  id="trspor02"
                  data-autoplay="true"
                  data-library="trspor02"
                  data-adobe-an="AdobeAnturksporuanimV2"
                  data-composition-code="9EEBA9FC8BB04B3AA5D4EDD0D06AF954"
                  data-script="https://s3.turkcell.com.tr/SiteAssets/Banner/html5/turksporu-anim-bannerV2/turksporu-anim-bannerV2.js"
                  data-component="CanvasBanner"
                />
              </div>
            </div>
          </div>
          <div
            data-background="../../../assetsv2/common/images/dummy/firsat-garaji-banner-bg2.jpg"
            className="swiper-slide swiper-lazy"
            data-location-href="/servisler/supercam?place=banner-3"
          >
            <div className="m-grid">
              <div className="m-grid-col-6">
                <div className="m-carousel__desc m-carousel__desc--text-shadow">
                  <h3>TURKSPORU Sahada</h3>
                  <p>
                    Türk sporuna dair her şey artık www.turksporu.com.tr’de!
                  </p>
                  <div className="m-btn-group m-btn-group--align-left">
                    <a className="a-btn" title="İncele" href="javascript:;">
                      İncele
                    </a>
                  </div>
                </div>
              </div>
              <div className="m-grid-col-6 text-right">
                <img
                  className="lazyload"
                  src="../../../assetsv2/common/images/spacer.gif"
                  alt=""
                  data-src="https://s2.turkcell.com.tr/SiteAssets/Cihaz/cep-telefonu/apple/6s-plus/cg/4/4_600x450.png"
                />
              </div>
            </div>
          </div>
          <div className="swiper-slide swiper-lazy bg_color_white">
            <div className="m-grid">
              <div className="m-grid-col-6 margin-auto text-center">
                <div className="m-carousel__desc pull-center">
                  <img
                    src="../../../assetsv2/common/images/dummy/iphonex-bg@2x.png"
                    width="505"
                    alt="Iphone X"
                  />
                  <h3 className="color_midnight">
                    iPhone X telefon ve aksesuarları şimdi turkcell.com.tr’de
                  </h3>
                  <div className="m-btn-group m-btn-group--m-align-center">
                    <a
                      className="a-btn a-btn--secondary"
                      title="İncele"
                      href="javascript:;"
                    >
                      İncele
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="swiper-slide swiper-lazy bg_color_alabaster">
            <div className="m-grid">
              <div className="m-grid-col-6 margin-auto text-center">
                <div className="m-carousel__desc pull-center">
                  <img
                    src="../../../assetsv2/common/images/dummy/hero-copy-7@2x.png"
                    width="198"
                    alt="Google Pixel"
                  />
                  <h3 className="color_midnight">
                    Pixel 3 telefon ve aksesuarları şimdi turkcell.com.tr’de
                  </h3>
                  <div className="m-btn-group m-btn-group--m-align-center">
                    <a
                      className="a-btn a-btn--secondary"
                      title="İncele"
                      href="javascript:;"
                    >
                      İncele
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            className="swiper-slide swiper-lazy bg_color_alabaster"
            data-swiper-autoplay="3000"
          >
            <div className="m-grid">
              <div className="m-grid-col-5">
                <div className="m-carousel__desc">
                  <img
                    src="../../../assetsv2/common/images/dummy/hero-copy-7@2x.png"
                    width="198"
                    alt="Google Pixel"
                  />
                  <h3 className="color_midnight">
                    Pixel 3 telefon ve aksesuarları şimdi turkcell.com.tr’de
                  </h3>
                  <div className="m-btn-group m-btn-group--align-left">
                    <a
                      className="a-btn a-btn--secondary"
                      title="İncele"
                      href="javascript:;"
                    >
                      İncele
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            data-background="https://s1.turkcell.com.tr/SiteAssets/Banner/html5/turkiyenin-uygulamalari-bannerV2/images/bg.jpg"
            className="swiper-slide swiper-lazy"
          >
            <video
              className="a-video-animate-banner"
              data-component="VideoBanner"
              data-autoplay="true"
              src="../../../assetsv2/common/images/dummy/turkcell-huawei-hediye-ts-banner_1920x500_v2.mp4"
            />
            <div className="m-grid">
              <div className="m-grid-col-5">
                <div className="m-carousel__desc m-carousel__desc--text-shadow">
                  <h3>
                    Birbirinden avantajlı okula dönüş fırsatları
                    turkcell.com.tr’de!
                  </h3>
                  <div className="m-btn-group m-btn-group--align-left m-btn-group--m-align-center">
                    <a className="a-btn" title="İncele" href="javascript:;">
                      İncele
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            data-background="../../../assetsv2/common/images/dummy/banner2.jpg"
            className="swiper-slide swiper-lazy"
            data-location-href="/servisler/supercam?place=banner-3"
          >
            <div className="m-grid">
              <div className="m-grid-col-5">
                <div className="m-carousel__desc m-carousel__desc--text-shadow">
                  <h3 className="uxr-slide-to-left">
                    Merakla beklenen PS4 oyunu God of War taksit imkanlarıyla
                    turkcell.com.tr’de
                  </h3>
                  <div className="m-btn-group m-btn-group--align-left m-btn-group--m-align-center">
                    <a className="a-btn" title="İncele" href="javascript:;">
                      İncele
                    </a>
                    <a
                      className="a-btn a-btn--with-icon icon-play a-btn--white"
                      data-fancybox
                      data-base-class="fancybox-video-mobile"
                      title="Oynat"
                      href="https://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4"
                    >
                      Oynat
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            data-background="../../../assetsv2/common/images/dummy/iphone-Xr-platinum-banner-bg.jpg"
            className="swiper-slide swiper-lazy"
          >
            <div className="m-grid">
              <div className="m-grid-col-5">
                <div className="m-carousel__desc m-carousel__desc--text-shadow">
                  <h3>
                    Birbirinden avantajlı okula dönüş fırsatları
                    turkcell.com.tr’de!
                  </h3>
                  <div className="m-btn-group m-btn-group--align-left m-btn-group--m-align-center">
                    <a className="a-btn" title="İncele" href="javascript:;">
                      İncele
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            data-background="../../../assetsv2/common/images/dummy/banner3.jpg"
            className="swiper-slide swiper-lazy"
          >
            <div className="m-grid">
              <div className="m-grid-col-5">
                <div className="m-carousel__desc m-carousel__desc--text-shadow">
                  <h3>Yenilenen Turkcell mağazaları hizmetinizde</h3>
                  <div className="m-btn-group m-btn-group--align-left m-btn-group--m-align-center">
                    <a
                      className="a-btn"
                      title="Detaylı Bilgi"
                      href="javascript:;"
                    >
                      Detaylı Bilgi
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            data-background="../../../assetsv2/common/images/dummy/banner3.jpg"
            className="swiper-slide swiper-lazy"
          >
            <div className="m-grid">
              <div className="m-grid-col-5">
                <div className="m-carousel__desc m-carousel__desc--text-shadow">
                  <h3>
                    Turkcell’e gelin, 12 ay boyunca ayda 1 GB internetinizi
                    gönlünüzce kullanın.
                  </h3>
                  <p>
                    Şimdi siz de Turkcell’e gelin, 12 ay boyunca ayda 1 GB
                    internetinizi gönlünüzce kullanın.
                  </p>
                  <div className="m-btn-group m-btn-group--align-left m-btn-group--m-align-center">
                    <a className="a-btn" title="Keşfet" href="javascript:;">
                      Keşfet
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            data-background="../../../assetsv2/common/images/dummy/banner3.jpg"
            className="swiper-slide swiper-lazy"
          >
            <div className="m-grid">
              <div className="m-grid-col-5">
                <div className="m-carousel__desc m-carousel__desc--text-shadow">
                  <h3>
                    Turkcell’e gelin, 12 ay boyunca ayda 1 GB internetinizi
                    gönlünüzce kullanın.
                  </h3>
                  <p>
                    Şimdi siz de Turkcell’e gelin, 12 ay boyunca ayda 1 GB
                    internetinizi gönlünüzce kullanın.
                  </p>
                  <div className="m-btn-group m-btn-group--align-left m-btn-group--m-align-center">
                    <a className="a-btn" title="Keşfet" href="javascript:;">
                      Keşfet
                    </a>
                    <a
                      className="a-btn a-btn--with-icon icon-play a-btn--white"
                      data-options='{"smallBtn":true}'
                      data-fancybox
                      data-base-class="fancybox-video-mobile"
                      title="Oynat"
                      href="../../../assetsv2/common/images/dummy/sample-video.mp4"
                    >
                      Oynat
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            data-background="../../../assetsv2/common/images/dummy/banner3.jpg"
            className="swiper-slide swiper-lazy"
          >
            <div className="m-grid">
              <div className="m-grid-col-7" />
              <div className="m-grid-col-5">
                <div className="m-carousel__desc m-carousel__desc--text-shadow">
                  <h3>
                    Turkcell’e gelin, 12 ay boyunca ayda 1 GB internetinizi
                    gönlünüzce kullanın.
                  </h3>
                  <div className="m-btn-group m-btn-group--align-left m-btn-group--m-align-center">
                    <a className="a-btn" title="Keşfet" href="javascript:;">
                      Keşfet
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container m-slider__container">
        <a
          className="a-btn-icon m-slider__prev a-btn-icon--circle"
          href="javascript:;"
          title="Geri"
        >
          <i className="icon-arrow-left" />
          Geri
        </a>
        <a
          className="a-btn-icon m-slider__next a-btn-icon--circle"
          href="javascript:;"
          title="İleri"
        >
          <i className="icon-arrow-right" />
          İleri
        </a>
      </div>
    </div>
    <div
      className="a-bullets"
      data-component='{"count":5,"arcHeight":6,"marginTop":338,"type":"Bullets"}'
    >
      <svg style={{ height: "100%" }}>
        <defs>
          <clipPath id="bullets-clip-path-117">
            <rect
              className="a-bullets__clip"
              x="0"
              y="0"
              width="0"
              height="0"
            />
          </clipPath>
          <linearGradient id="bullets-stroke-gradient">
            <stop offset="0%" stopColor="#ffc900" />
            <stop offset="100%" stopColor="#eeb116" />
          </linearGradient>
        </defs>
        <path className="a-bullets__bg" d="" />
        <g clipPath="url(#bullets-clip-path-117)">
          <path
            className="a-bullets__item"
            d=""
            stroke="url(#bullets-stroke-gradient)"
          />
        </g>
      </svg>
      <ul className="m-flex" />
    </div>
  </section>
)
export default CarouselMobile

/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"

import { ReactComponent as IconMusic } from "../../../assets/images/icons/music.svg"
import { ReactComponent as IconPlay } from "../../../assets/images/icons/play-circle.svg"
import { ReactComponent as IconWhatsapp } from "../../../assets/images/icons/whatsapp.svg"

const ProductDetailCarousel = () => {
  return (
    <section
      id="similar-products"
      className="product-detail__similar-products white"
    >
      <div className="container">
        <h2 className="text-center compact-title m-b-10">
          16 GB ile Neler Yapabilirim?
        </h2>
        <div
          className="m-slider product-card-slider"
          data-component='{"slidesPerView":3,"spaceBetween":30,"navigation":true,"pagination":true,"type":"Slider"}'
        >
          <div className="m-slider__swiper swiper-container">
            <div className="swiper-wrapper">
              <div className="swiper-slide swiper-lazy">
                <a
                  href="#"
                  className="m-card m-card--misc m-card--misc--dark-blue m-card--misc--full-theme"
                >
                  <div className="m-card__head">
                    <IconMusic />
                  </div>
                  <div className="m-card__body">
                    <h3 className="m-card__title">Müzik</h3>
                    <p>Lorem ipsum dolor sit</p>
                  </div>
                  <div className="m-card__foot">198 saat dinleme</div>
                </a>
              </div>

              <div className="swiper-slide swiper-lazy">
                <a
                  href="#"
                  className="m-card m-card--misc m-card--misc--blue m-card--misc--full-theme"
                >
                  <div className="m-card__head">
                    <IconPlay />
                  </div>
                  <div className="m-card__body">
                    <h3 className="m-card__title">Video</h3>
                    <p>Lorem ipsum dolor sit</p>
                  </div>
                  <div className="m-card__foot">102 saat izleme</div>
                </a>
              </div>

              <div className="swiper-slide swiper-lazy">
                <a
                  href="#"
                  className="m-card m-card--misc m-card--misc--green m-card--misc--full-theme"
                >
                  <div className="m-card__head">
                    <IconWhatsapp />
                  </div>
                  <div className="m-card__body">
                    <h3 className="m-card__title">Whatsapp</h3>
                    <p>Lorem ipsum dolor sit</p>
                  </div>
                  <div className="m-card__foot">400 mesaj</div>
                </a>
              </div>

              <div className="swiper-slide swiper-lazy">
                <a
                  href="#"
                  className="m-card m-card--misc m-card--misc--dark-blue m-card--misc--full-theme"
                >
                  <div className="m-card__head">
                    <IconMusic />
                  </div>
                  <div className="m-card__body">
                    <h3 className="m-card__title">Müzik</h3>
                    <p>Lorem ipsum dolor sit</p>
                  </div>
                  <div className="m-card__foot">198 saat dinleme</div>
                </a>
              </div>

              <div className="swiper-slide swiper-lazy">
                <a
                  href="#"
                  className="m-card m-card--misc m-card--misc--blue m-card--misc--full-theme"
                >
                  <div className="m-card__head">
                    <IconPlay />
                  </div>
                  <div className="m-card__body">
                    <h3 className="m-card__title">Video</h3>
                    <p>Lorem ipsum dolor sit</p>
                  </div>
                  <div className="m-card__foot">102 saat izleme</div>
                </a>
              </div>

              <div className="swiper-slide swiper-lazy">
                <a
                  href="#"
                  className="m-card m-card--misc m-card--misc--green m-card--misc--full-theme"
                >
                  <div className="m-card__head">
                    <IconWhatsapp />
                  </div>
                  <div className="m-card__body">
                    <h3 className="m-card__title">Whatsapp</h3>
                    <p>Lorem ipsum dolor sit</p>
                  </div>
                  <div className="m-card__foot">400 mesaj</div>
                </a>
              </div>
            </div>
          </div>
          <div className="container m-slider__container">
            <a
              className="a-btn-icon m-slider__prev a-btn-icon--circle"
              href="javascript:;"
              title="Geri"
            >
              <i className="icon-arrow-left" />
              Geri
            </a>
            <a
              className="a-btn-icon m-slider__next a-btn-icon--circle"
              href="javascript:;"
              title="İleri"
            >
              <i className="icon-arrow-right" />
              İleri
            </a>
          </div>
          <div className="swiper-pagination" />
        </div>
      </div>
    </section>
  )
}

export default ProductDetailCarousel

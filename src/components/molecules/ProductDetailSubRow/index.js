import React from "react"

const ProductDetailSubRow = () => {
  return (
    <section
      id="product-detail-subrow"
      className="bg_color_alice p-t-20 p-b-20 text-center"
    >
      <i className="a-icon icon-account-regular m-r-10" />
      <small className="font-400 m-b-0">
        <strong>Turkcell’li misiniz?</strong>
      </small>
      <small className="color_cerulean font-400 m-l-30 m-b-0">
        Size özel fırsatları görmek için <strong>giriş yapın.</strong>
      </small>
    </section>
  )
}

export default ProductDetailSubRow

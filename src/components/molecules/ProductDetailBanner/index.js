import React from "react"

const ProductDetailBanner = () => {
  return (
    <section id="product-dtail-banner">
      <div className="container">
        <div className="m-offer-banner">
          <div className="m-grid align-items-center justify-content-between">
            <div className="m-grid-col-6 m-grid-offset-1">
              <div className="m-offer-banner__content">
                <h4>Neden Bu Paketi Almalıyım?</h4>
                <p>
                  Sed pharetra vel nibh feugiat maximus. Sed turpis velit,
                  lobortis et orci ut, condimentum gravida dui. Pellentesque
                  vulputate urna eu mi rutrum placerat. Aliquam eu rhoncus
                  lorem. Suspendisse tempor odio et auctor bibendum.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
  )
}

export default ProductDetailBanner

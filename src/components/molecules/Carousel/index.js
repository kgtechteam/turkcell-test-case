/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"

const Carousel = () => (
  <div className="product-list__banner">
    <div
      className="m-slider"
      data-component='{"slidesPerView":1,"spaceBetween":0,"centeredSlides":true,"pagination":true,"canHidePagination":true,"navigation":true,"lazy":{"loadPrevNext":true},"type":"Slider"}'
    >
      <div className="m-slider__swiper swiper-container">
        <div className="swiper-wrapper">
          <div
            data-background="../../../assetsv2/common/images/dummy/product-banner.jpg"
            className="swiper-slide swiper-lazy m-product-banner m-carousel-gradient--blue"
            data-swiper-autoplay="4000"
            data-location-href="/servisler/supercam?place=banner-3"
          >
            <div className="container">
              <div className="m-grid">
                <div className="m-grid-col-6">
                  <div className="m-carousel__desc">
                    <h3 data-animation="slideInLeft">
                      DJI Mavic Pro 36 aya varan taksit imkanları ile
                      Turkcell’de.
                    </h3>
                    <div className="m-btn-group m-btn-group--align-left m-btn-group--m-align-center">
                      <a className="a-btn" title="İncele" href="javascript:;">
                        İncele
                      </a>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div
            data-background="https://s3.turkcell.com.tr/SiteAssets/Banner/html5/turksporu-anim-bannerV2/images/bg.jpg"
            className="swiper-slide swiper-lazy"
            data-location-href="/servisler/supercam?place=banner-3"
          >
            <div className="container">
              <div className="m-grid">
                <div className="m-grid-col-6">
                  <div className="m-carousel__desc">
                    <h3 data-animation="slideInLeft">TURKSPORU Sahada</h3>
                    <p data-animation="slideInLeft">
                      Türk sporuna dair her şey artık www.turksporu.com.tr’de!
                    </p>
                    <div className="m-btn-group m-btn-group--align-left">
                      <a className="a-btn" title="İncele" href="javascript:;">
                        İncele
                      </a>
                    </div>
                  </div>
                </div>
                <div className="m-grid-col-6">
                  <img
                    className="lazyload"
                    src="../../../assetsv2/common/images/spacer.gif"
                    alt=""
                    data-src="../../../assetsv2/common/images/dummy/service-img.png"
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container m-slider__container">
        <a
          className="a-btn-icon m-slider__prev a-btn-icon--circle"
          href="javascript:;"
          title="Geri"
        >
          <i className="icon-arrow-left" />
          Geri
        </a>
        <a
          className="a-btn-icon m-slider__next a-btn-icon--circle"
          href="javascript:;"
          title="İleri"
        >
          <i className="icon-arrow-right" />
          İleri
        </a>
      </div>
      <div className="swiper-pagination" />
    </div>
  </div>
)

export default Carousel

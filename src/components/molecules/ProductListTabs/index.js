/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"
import "./index.scss"

const ProductListTabs = () => (
  <section id="tabs" className="product-list__tabs">
    <div className="container">
      <div
        className="m-slider m-slider__tabs m-slider__tabs--iconless"
        data-component='{"slidesPerView":8,"spaceBetween":10,"navigation":true,"type":"Slider"}'
      >
        <div className="m-slider__swiper swiper-container">
          <div className="swiper-wrapper">
            <div className="swiper-slide swiper-lazy">
              <a href="javascript:;" className="tab--active">
                Akıllı Telefonlar
              </a>
            </div>
            <div className="swiper-slide swiper-lazy">
              <a href="javascript:;">Tablet ve Bilgisayarlar</a>
            </div>
            <div className="swiper-slide swiper-lazy">
              <a href="javascript:;">Telefon Aksesuarları</a>
            </div>
            <div className="swiper-slide swiper-lazy">
              <a href="javascript:;">Televizyon</a>
            </div>
            <div className="swiper-slide swiper-lazy">
              <a href="javascript:;">Araç &amp; Seyahat</a>
            </div>
            <div className="swiper-slide swiper-lazy">
              <a href="javascript:;">Ses &amp; Müzik</a>
            </div>
            <div className="swiper-slide swiper-lazy">
              <a href="javascript:;">Giyilebilir Teknolojiler</a>
            </div>
            <div className="swiper-slide swiper-lazy">
              <a href="javascript:;">Hobi &amp; Oyun</a>
            </div>
            <div className="swiper-slide swiper-lazy">
              <a href="javascript:;">Televizyon</a>
            </div>
            <div className="swiper-slide swiper-lazy">
              <a href="javascript:;">Televizyon</a>
            </div>
            <div className="swiper-slide swiper-lazy">
              <a href="javascript:;">Televizyon</a>
            </div>
          </div>
        </div>
        <div className="container m-slider__container">
          <a
            className="a-btn-icon m-slider__prev a-btn-icon--circle"
            href="javascript:;"
            title="Geri"
          >
            <i className="icon-arrow-left" />
            Geri
          </a>
          <a
            className="a-btn-icon m-slider__next a-btn-icon--circle"
            href="javascript:;"
            title="İleri"
          >
            <i className="icon-arrow-right" />
            İleri
          </a>
        </div>
      </div>
    </div>
  </section>
)

export default ProductListTabs

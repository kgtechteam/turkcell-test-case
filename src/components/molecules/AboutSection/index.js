/* eslint-disable react/prop-types */
/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"

const AboutSection = ({ title, content }) => (
  <div className="m-about-device">
    <h2 className="compact-title">{title}</h2>
    {content.map(item => (
      <div
        className="toggle-content"
        data-component="ToggleContent"
        data-char-limit=""
      >
        <h4>{item.subTitle}</h4>
        <p className="toggle-content__block">{item.summary}</p>
        <div className="toggle-content__hidden">
          <p>{item.longText}</p>
        </div>
        <p>
          <a
            className="toggle-content__button js-toggle-button"
            href="javascript:;"
          >
            Daha Fazla <i className="a-icon icon-arrow-left" />{" "}
          </a>
        </p>
      </div>
    ))}
  </div>
)

export default AboutSection

/* eslint-disable no-script-url */
/* eslint-disable jsx-a11y/anchor-is-valid */
import React from "react"

const TabSliderMobile = () => (
  <section id="tabs" className="product-list__tabs m-b-0">
    <h1 className="text-center">Cihazlar</h1>
    <div
      className="m-slider m-slider__tabs m-slider__tabs--iconless"
      data-component='{"slidesPerView":"auto","spaceBetween":10,"navigation":true,"type":"Slider"}'
    >
      <div className="m-slider__swiper swiper-container">
        <div className="swiper-wrapper">
          <div className="swiper-slide swiper-lazy">
            <a href="javascript:;" className="tab--active">
              Tümü
            </a>
          </div>
          <div className="swiper-slide swiper-lazy">
            <a href="javascript:;">Kılıf &amp; Ekran Koruyucuları</a>
          </div>
          <div className="swiper-slide swiper-lazy">
            <a href="javascript:;">Şarj Üniteleri</a>
          </div>
        </div>
      </div>
      <div className="container m-slider__container">
        <a
          className="a-btn-icon m-slider__prev a-btn-icon--circle"
          href="javascript:;"
          title="Geri"
        >
          <i className="icon-arrow-left" />
          Geri
        </a>
        <a
          className="a-btn-icon m-slider__next a-btn-icon--circle"
          href="javascript:;"
          title="İleri"
        >
          <i className="icon-arrow-right" />
          İleri
        </a>
      </div>
    </div>
  </section>
)

export default TabSliderMobile

import React from "react"

const ProductDetailFixedBottomBar = () => {
  return (
    <div
      id="prouduct-detail-fixed-bottom"
      className="prouduct-detail-fixed-bottom bg_color_white fixed"
    >
      <div className="fixed-bar fixed-bar--bottom">
        <span className="fixed-bar__price m-l-10">
          109.88 <sup>TL X 24 AY</sup>
        </span>
        <button className="a-btn m-0 m-r-10">Sepete Devam Et</button>
      </div>
    </div>
  )
}

export default ProductDetailFixedBottomBar

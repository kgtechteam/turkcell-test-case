import React from "react"

import ProductVideo from "../../../assets/images/dummy/product-video.jpg"

const ProductDetailVideoMobile = () => {
  return (
    <section id="product-social-video-content" className="white">
      <div
        className="m-product-detail-info"
        data-component='{"lessText":"Daha Az","scrollToTop":true,"type":"MoreContent"}'
      >
        <div className="js-more-container">
          <article className="js-more-content__first p-b-0">
            <div className="container">
              <div className="clearfix m-flex justify-content-center align-items-center">
                <div className="text-left">
                  <h2>Bu Paketi Neden Almalıyım</h2>
                  <p className="w-100">
                    Lorem ipsum dolor sit amet consectetuer.Lorem ipsum dolor
                    sit amet consectetuer.Lorem ipsum dolor sit amet
                    consectetuer. Lorem ipsum dolor sit amet consectetuer. Lorem
                    ipsum dolor sit amet consectetuer.Lorem ipsum dolor sit amet
                    consectetuer.
                  </p>
                </div>
              </div>
            </div>

            <div id="product-social-video">
              <div className="m-grid">
                <div className="m-grid-col m-grid-offset-1 m-grid-offset-right-1">
                  <div
                    className="m-product-video-banner lazyloaded"
                    data-background={ProductVideo}
                    style={{ backgroundImage: `url(${ProductVideo})` }}
                  >
                    <a
                      className="a-btn-icon a-btn-icon--big a-btn-icon--circle a-btn-icon--circle--yellow"
                      data-fancybox=""
                      data-base-class="fancybox-video-mobile"
                      href="https://download.blender.org/peach/bigbuckbunny_movies/BigBuckBunny_320x180.mp4"
                    >
                      <i className="icon-play" />
                    </a>
                    <p className="color_white bold">
                      LG V30+ Deneyimini Keşfet
                    </p>
                  </div>
                </div>
              </div>
            </div>
          </article>
        </div>
      </div>
    </section>
  )
}

export default ProductDetailVideoMobile

import React from "react"

const ProductDetailCheckbox = () => {
  return (
    <div>
      <div className="a-price-radio-b m-t-30">
        <input
          type="radio"
          name="price"
          checked
          id="s0-0-0-9-name"
          data-parsley-multiple="price"
        />
        <label className="a-price-radio-b__label" htmlFor="s0-0-0-9-name">
          <div className="a-price-radio-b__label__top">
            <span className="a-price-radio-b__title">Yıllık Abonelik</span>
            <span className="a-price a-price--gray">
              59,90
              <sup>
                <span className="a-price__currency">TL</span>
              </sup>
            </span>
          </div>
          <div className="a-price-radio-b__note with-package-info">
            <span className="a-price__package-info">
              50 <sup>GB</sup>
            </span>
            <span className="color_cobalt1">x12 Ay</span>
          </div>
        </label>
      </div>

      <div className="a-price-radio-b">
        <input type="radio" name="price" id="s0-0-0-10-name" />
        <label className="a-price-radio-b__label" htmlFor="s0-0-0-10-name">
          <div className="a-price-radio-b__label__top">
            <span className="a-price-radio-b__title">Aylık Abonelik</span>
            <span className="a-price a-price--gray">
              59,90
              <sup>
                <span className="a-price__currency">TL</span>
              </sup>
            </span>
          </div>
          <div className="a-price-radio-b__note with-package-info">
            <span className="a-price__package-info">
              25 <sup>GB</sup>
            </span>
            <span className="color_cobalt1">x Ay</span>
          </div>
        </label>
      </div>
    </div>
  )
}

export default ProductDetailCheckbox

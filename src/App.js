import React from "react"
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom"
import { ProductDetail, ProductList } from "pages"

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route path="/product-list">
            <ProductList />
          </Route>
          <Route path="/product-detail">
            <ProductDetail />
          </Route>
          <Route path="/">
            <Redirect to="/product-list" />
          </Route>
        </Switch>
      </Router>
    </div>
  )
}

export default App

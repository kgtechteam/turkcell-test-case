### KG Teknoloji Test Case

---

#### Tech Stack

- React
- Sass

Existing components regenerated with React. We could not regenerated all the components due to time. At a larger time, all components can be rearrange according to their intended purpose.

#### Available Scripts

**yarn start** - Start project in local
**yarn build** - Build for production

ProductList - http://localhost:3000/product-list
ProductDetail - http://localhost:3000/product-detail

For mobile please select device from browser developer tools.
